## Synopsis

Air quality monitor software for STM32L476 based project.  
Project uses CO, CO2, PM2.5/10, temperature, humidity, and pressure sensors.  
Display is 1.3" OLED.  
Power consumption is ~0.9W so will run for 12hr on a few 18650's.  

## Warning

This project is a work in process.  
Drivers that are working: SD, CO, CO2, PM, humidity, pressure, OLED.  
Drivers that need work: WiFi, graph.  

## Hardware

![aqm_unit](/uploads/d1090973e680c27ca554a031d875c1b1/aqm_unit.jpg)  

Hardware design in Altium will be uploaded soon: https://gitlab.com/thmjpr/Air_Quality_HW  
Improvements can be made: VOC sensor, more compact size, etc.  


## License

MIT license where applicable.  