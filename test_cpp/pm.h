/**
  ******************************************************************************
  * @file    pm.h
  * @author  Thomas
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * 
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "stdbool.h"
#include "stdint.h"
#include "PM_PMS7003.h"
#include "PM_SDS0x_sensor.h"


/* Functions -----------------------------------------------------------------*/

class PARTICULATE
{

public:
	PARTICULATE(UART_HandleTypeDef * uart_ptr);
	~PARTICULATE();
	
	typedef enum
	{
		NONE,
		SDS_021,
		PMS7003,
	}PM_TYPE;
	
	
	PM_TYPE sensor();
	
	/**
	* @brief Is sensor present
	* @param  
	* @retval  
	*/
	bool is_present();
	
	/**
	* @brief Initialzation of particulate sensor
	* @param  
	* @retval  
	*/
	bool init(void);
	
	/**
	* @brief Power on particulate sensor (5V)
	* @param  
	* @retval  
	* @note	
	*/
	void power(bool power);

	/**
	* @brief Perform blocking read of PM10 level
	* @param  
	* @retval  PM10 level (ug/m^3 concentration)
	* @note Can take ~10-60ms to respond
	*/
	bool read_blocking(uint32_t * pm10);
	
	/**
	* @brief  Run this every ~1s to send request for PM data
	* @param  
	* @retval  
	*/
	void request(void);
	
	/**
  * @brief  Retrieve PM10 level
  * @param  None
  * @retval PM1.0 level (ug/m^3 concentration)
  */
	float get_pm1_level();
	
	/**
  * @brief  Retrieve PM10 level
  * @param  None
  * @retval PM2.5 level (ug/m^3 concentration)
  */
	float get_pm25_level();
	
	/**
  * @brief  Retrieve PM2.5 level
  * @param  None
  * @retval PM10 level (ug/m^3 concentration)
  */
	float get_pm10_level();

private:
	PM_TYPE sensor_type;
	UART_HandleTypeDef * uart;
	
	PM_PLANT pm_plant;
	SDS_PM pm_sds;
};


/*-------------------------------END OF FILE-------------------------------*/