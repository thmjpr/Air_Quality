/**
******************************************************************************
* @file		config.h
* @author	Thomas
* @version
* @date		Dec 2016
* @brief	Saved configuration information

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stdint.h"


/*			------------------------------------------------------------------*/

class CONFIG
{
	enum Configuration {	//??
		Status,
		Time,
		LED,
		NTP,
		Shutdown,
	};

public:
	CONFIG();
	~CONFIG();

	/**
	* @brief 
	* @param
	* @retval
	*/
	bool init(void);

	//Config settings and their default value
	bool button_beep = true;
	bool audio_alarm = true;			//Alarm if dangerous thresholds exceeded

	char wifi_ap[10] = "test_ap";
	char wifi_pw[10] = "test_pw";

	char ntp_server[20] = "0.0.0.com";
	char server[20] = "www.com";

	//limits? could be in the files of sensors themselves
	uint32_t co_alarm_ppm = 200;
	uint32_t co2_alarm_ppm = 1500;
	uint32_t pm2_alarm = 100;			//PM2.5 particulate level limit
	uint32_t pm10_alarm = 100;			//PM10 particulate level limit




private:
	

};



