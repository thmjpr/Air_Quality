/**
  ******************************************************************************
  * @file    sd_card.cpp
  * @author  Thomas
  * @version 
  * @date    
  * @brief   Manage log storage to SD card
  ******************************************************************************
  * @attention
  * 
  * 
  ******************************************************************************
  */

#include "sd_card.h"
#include "stdio.h"
#include "string.h"
#include "main.h"
#include "ff_gen_drv.h"
#include "sd_diskio.h"

#include "IniReader\ini.h"


//---------------------------------------------------------
const char header[] = "Date,Time,Temperature (C),Humidity (%),Pressure (Pa),CO (ppb),CO2 (ppm),PM1.0,PM2.5,PM10,-,Status";
const char csv[] = "MM-DD-YY,HH-MM-SS,0.0,0-100,0-x,0-x000 ppm, 0-x0000 ppm, 0-x000, 0-x000, 0-x000";
SD_HandleTypeDef hsd1;
SD_CardInfo SDCardInfo1;

//---------------------------------------------------------
SD_CARD::SD_CARD()
{
	present = false;
}

//---------------------------------------------------------
SD_CARD::~SD_CARD()
{
	shutdown();
}

bool SD_CARD::init()
{	
	hsd1.Instance =				SDMMC1;
	hsd1.Init.ClockEdge =		SDMMC_CLOCK_EDGE_RISING;
	hsd1.Init.ClockBypass =		SDMMC_CLOCK_BYPASS_DISABLE;
	hsd1.Init.ClockPowerSave =	SDMMC_CLOCK_POWER_SAVE_DISABLE;
	hsd1.Init.BusWide =			SDMMC_BUS_WIDE_1B;
	hsd1.Init.HardwareFlowControl = SDMMC_HARDWARE_FLOW_CONTROL_DISABLE;
	hsd1.Init.ClockDiv =		2;				//32MHz divider, go lower for debugging
	
	//Calls msp init and initializes low level hardware (pins)
	if (HAL_SD_Init(&hsd1, &SDCardInfo1) != SD_OK)			
		present = false;
	else
		present = true;
	
  //FatFS: Link the SD driver
	//maybe dont link if its not present?
	retSD = FATFS_LinkDriver(&SD_Driver, SD_Path);
	
	return present;
}

//---------------------------------------------------------
void SD_CARD::shutdown()
{
	present = false;
	f_close(&LogFile);
	f_close(&ConfigFile);
	f_mount(NULL, (TCHAR const*)SD_Path, 0);
	FATFS_UnLinkDriver(SD_Path);
}


//---------------------------------------------------------
void SD_CARD::write_csv_header(FIL * log_file)
{ 
	unsigned int bytes;
	char date[] = __DATE__;
	char fw[80] = ",Air quality log - Firmware: ";
	
	strcat(fw, date);
	
	f_write(log_file, header, strlen(header), &bytes);
	f_write(log_file, fw, strlen(fw), &bytes);
}

//---------------------------------------------------------
void SD_CARD::write_new_line(FIL * log_file, char * str)
{
	unsigned int bytes;

	f_write(log_file, "\n", 1, &bytes);
	f_write(log_file, str, strlen(str), &bytes);
}

//---------------------------------------------------------
void SD_CARD::save_log(SD_CARD::logData * log)
{
	static int i = 0;
	
	if(present)
		sanitize_and_write(log);
	
	//force data write every 10 lines
	if (i++ > 10)
	{
		i = 0;
		f_sync(&LogFile);
	}
}

//---------------------------------------------------------
int32_t SD_CARD::check_sd_formatted(RTC_DateTypeDef * nDate)
{
	FRESULT res;
	//uint32_t byteswritten, bytesread;
	char FileName[30] = "Log_";
	
	present = false;

	//If disk IO driver linked OK
	if (retSD != 0)
		return -1;		//could return string error? "driver io fail" etc.
	
		//Register the files system object to the FatFS module
	if (f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0) != FR_OK)
		return -1;
	
	//Form filename
	sprintf(strchr(FileName, '\0'), "20%02d-%02d-%02d", nDate->Year, nDate->Month, nDate->Date);
	sprintf(strchr(FileName, '\0'), ".csv");
	
	//Create new file if it doesn't exist, open for writing
	if (f_open(&LogFile, FileName, FA_OPEN_EXISTING | FA_WRITE) != FR_OK)
	{
		if (f_open(&LogFile, FileName, FA_CREATE_NEW | FA_WRITE) != FR_OK)
			return -1;
		else
			write_csv_header(&LogFile);	//New file is created so write header to top of file.
	}
	else		//File exists
	{
		if (f_size(&LogFile) < 100)
		{
			//problem with file
		}		
		
		res = f_lseek(&LogFile, f_size(&LogFile));		// Move to end of the file to append data
	}

	present = true;
	return 0;	//success
}


int32_t SD_CARD::load_config_file(CONFIG * config)
{
	char FileName[] = "AQM_Config.ini";
	
	//Create new file if it doesn't exist, open for writing
	if (f_open(&ConfigFile, FileName, FA_OPEN_EXISTING | FA_WRITE) != FR_OK)	//if file cant be opened
	{
		if (f_open(&ConfigFile, FileName, FA_CREATE_NEW | FA_WRITE) != FR_OK)	//if new file cant be created
			return -1;
		else
		{
			//Write default config data

		}
	}
	else		//File exists
	{
		//INIReader reader(&ConfigFile);
		//get config data
	}

	present = true;
	return 0;	//success
}


//---------------------------------------------------------
//if sensor data invalid then put "-"
void SD_CARD::sanitize_and_write(logData * log)
{
	char str[150] = "";	//temp
	
	
	//Date		YYYY-MM-DD
	sprintf(strchr(str, '\0'), "20%02d-%02d-%02d,", log->date.Year, log->date.Month, log->date.Date);
	
	//Time		HH:MM:SS
	sprintf(strchr(str, '\0'), "%02d:%02d:%02d,", log->time.Hours, log->time.Minutes, log->time.Seconds);
	
	//Temperature
	if ((log->temperature < -100) || (log->temperature > 500))
		sprintf(strchr(str, '\0'), "-,");
	else
		sprintf(strchr(str, '\0'), "%3.1f,", log->temperature);
	
	//Humidity
	if ((log->humidity < 1) || (log->humidity > 100))
		sprintf(strchr(str, '\0'), "-,");
	else
		sprintf(strchr(str, '\0'), "%03d,", log->humidity);
	
	//Pressure
	if ((log->pressure < 1) || (log->pressure > 300000))
		sprintf(strchr(str, '\0'), "-,");
	else
		sprintf(strchr(str, '\0'), "%03d,", log->pressure);

	//CO
	if ((log->co < 0) || (log->co > (1000 * 1000)))
		sprintf(strchr(str, '\0'), "-,");
	else
		sprintf(strchr(str, '\0'), "%03d,", log->co);
	
	//CO2
	if ((log->co2 < 1) || (log->co2 > 50000))
		sprintf(strchr(str, '\0'), "-,");
	else
	sprintf(strchr(str, '\0'), "%03d,", log->co2);

	//PM1.0
	if ((log->pm1 < 0) || (log->pm1 > 2000))
		sprintf(strchr(str, '\0'), "-,");
	else
		sprintf(strchr(str, '\0'), "%3.1f,", log->pm1);
	
	//PM2.5
	if ((log->pm2 < 0) || (log->pm2 > 2000))
		sprintf(strchr(str, '\0'), "-,");
	else
		sprintf(strchr(str, '\0'), "%3.1f,", log->pm2);

	//PM10
	if ((log->pm10 < 0) || (log->pm10 > 2000))
		sprintf(strchr(str, '\0'), "-,");
	else
		sprintf(strchr(str, '\0'), "%3.1f,", log->pm10);
	
	write_new_line(&LogFile, str);
	//f_printf also an option
}