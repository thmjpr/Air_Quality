/**
  ******************************************************************************
  * @file    pm.cpp
  * @author  Thomas
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  * 
  * 
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "pm.h"

//******************************************************************************
PARTICULATE::PARTICULATE(UART_HandleTypeDef * uart_ptr) : pm_plant(uart_ptr), pm_sds(uart_ptr)
{
}

//******************************************************************************
PARTICULATE::~PARTICULATE()
{
}

//******************************************************************************
bool PARTICULATE::is_present()
{
	
	
}

//-----------------------------------------------------------------------------
/**
  * @brief  Power on the 5V supply to external PM sensor
  * @param  
  * @retval  
  */
void PARTICULATE::power(bool power)
{

}

//******************************************************************************
/**
  * @brief  Initialize the PM sensor (power it up and check its present)
  * @param  
  * @retval  
  * @details 
  */

bool PARTICULATE::init(void)
{
	uint32_t pm10;
	
	if (pm_plant.init())
	{
		sensor_type = PMS7003;
		//delete pm_sds;
	}
	
	else if (pm_sds.init())
	{
		sensor_type = SDS_021;
	}
	
	else
	{
	}
	
}

//-----------------------------------------------------------------------------
void PARTICULATE::request(void)
{
	switch (sensor_type)
	{
	case SDS_021:
		pm_sds.request();
		break;
	case PMS7003:
		pm_plant.request();
		break;
	default:
		break;
	}
}

//-----------------------------------------------------------------------------

bool PARTICULATE::read_blocking(uint32_t * pm10)
{	
	
}

//-----------------------------------------------------------------------------
float PARTICULATE::get_pm25_level()
{

}

//-----------------------------------------------------------------------------
float PARTICULATE::get_pm10_level()
{

}

//-----------------------------------------------------------------------------
float PARTICULATE::get_pm1_level()
{

}
