/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
#include "stm32l4xx_hal.h"



void Error_Handler()
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */


	//ssd1306_display_string(40, 0, (uint8_t*)"ERROR", FONT_MED, 1);

	while (1)
	{
		HAL_Delay(100);
		//Log error somewhere
	}
	/* USER CODE END Error_Handler */ 
}