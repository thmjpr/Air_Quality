/**
  ******************************************************************************
  * @file    rtc.cpp
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */



/* Includes ------------------------------------------------------------------*/
#include "rtc.h"


RTC_TimeTypeDef cTime;			//Current time
RTC_DateTypeDef cDate;			//Current date

RTC_TimeTypeDef nTime;			//Updated time
RTC_DateTypeDef nDate;			//Updated date

RTC_HandleTypeDef hrtc;

/* get time and date function */
void rtc_get_time_and_date(RTC_TimeTypeDef *time, RTC_DateTypeDef *date)
{

	HAL_RTC_GetTime(&hrtc, time, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, date, RTC_FORMAT_BIN);	//Must call date after time, to ensure sync
	
}

/**
  * @brief  Get time function required for FafFS to use correct timestamp
  * @param  None
  * @retval None
  * @details	Pack binary RTC time into 32-bit value for use in FatFS
  *				http://elm-chan.org/fsw/ff/en/fattime.html
  */
DWORD get_fattime(void)
{
	DWORD cur = 0;
	
	rtc_get_time_and_date(&cTime, &cDate);
	
	cur = (uint8_t)(cDate.Year + 20) << 25;
	cur |= (cDate.Month) << 21;
	cur |= (cDate.Date) << 16;
	cur |= (cTime.Hours) << 11;
	cur |= (cTime.Minutes) << 5;
	cur |= (cTime.Seconds) << 0;
		
	return cur;
}


/* RTC init function */
void MX_RTC_Init(void)
{
	RTC_AlarmTypeDef sAlarm;
 /**Initialize RTC and set the Time and Date 	    */
	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 127;
	hrtc.Init.SynchPrediv = 255;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&hrtc) != HAL_OK)
		Error_Handler();

	HAL_RTC_GetTime(&hrtc, &cTime, RTC_FORMAT_BIN);		//Must call GetDate after GetTime
	HAL_RTC_GetDate(&hrtc, &cDate, RTC_FORMAT_BIN);
	
	
	//If RTC has not been initialized yet, set to Jan 2016
	if (cDate.Year < 16)
	{
		cTime.Hours = 12;
		cTime.Minutes = 26;
		cTime.Seconds = 0;
		cTime.DayLightSaving = RTC_DAYLIGHTSAVING_SUB1H;
		cTime.StoreOperation = RTC_STOREOPERATION_RESET;
		if (HAL_RTC_SetTime(&hrtc, &cTime, RTC_FORMAT_BIN) != HAL_OK)
			Error_Handler();

		cDate.WeekDay = RTC_WEEKDAY_SATURDAY;
		cDate.Month = RTC_MONTH_MARCH;
		cDate.Date = 11;
		cDate.Year = 17;

		if (HAL_RTC_SetDate(&hrtc, &cDate, RTC_FORMAT_BIN) != HAL_OK)
			Error_Handler();		
	}

	//Enable the Alarm A
	//Interrupt will occur at 0:0:0 every day
	sAlarm.AlarmTime.Hours = 0x0;
	sAlarm.AlarmTime.Minutes = 0x0;
	sAlarm.AlarmTime.Seconds = 0x0;
	sAlarm.AlarmTime.SubSeconds = 0x0;
	sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
	sAlarm.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;						//Mask weekday, alarm should occur every day
	sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
	sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
	sAlarm.AlarmDateWeekDay = 1;
	sAlarm.Alarm = RTC_ALARM_A;
	if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
		Error_Handler();

	//Enable the Alarm B
	//Interrupt will occur every second
	sAlarm.AlarmTime.SubSeconds = 0;
	sAlarm.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY | RTC_ALARMMASK_HOURS | RTC_ALARMMASK_MINUTES | RTC_ALARMMASK_SECONDS;
	sAlarm.AlarmDateWeekDay = 1;
	sAlarm.Alarm = RTC_ALARM_B;
	if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
		Error_Handler();
	
	/**Enable the WakeUp set to every 1 second
	*//*
	if (HAL_RTCEx_SetWakeUpTimer(&hrtc, 0, RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
	{
		Error_Handler();
	}*/
}

/* set time and date function */
void rtc_set_time_and_date(RTC_TimeTypeDef *time, RTC_DateTypeDef *date)
{
	if (HAL_RTC_SetTime(&hrtc, time, RTC_FORMAT_BIN) != HAL_OK)
		Error_Handler();
	if (HAL_RTC_SetDate(&hrtc, date, RTC_FORMAT_BIN) != HAL_OK)
		Error_Handler();		
}

/**
* @brief  RTC wakeup timer callback, every 1s
* @param  hrtc: Real time clock handle
* @retval None
*/
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
}

/**
* @brief  Alarm A event callback
* @param  hrtc: Real time clock handle
* @note   Occurs every 24hr
* @retval None
*/
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
	if (read_timer_)
		new_day = true;
}

/**
* @brief  Alarm B event callback
* @param  hrtc: Real time clock handle
* @note   Occurs every 1 second
* @retval None
*/
void HAL_RTCEx_AlarmBEventCallback(RTC_HandleTypeDef *hrtc)
{
	static int count = 0;

	if (read_timer_)
	{
		count++;
		if (count % 1 == 0)
			new_sensor_read = true;				//1s interval sensor update

		if (count % 5 == 0)						//5s interval CO2 + PM update
		{
			new_co2_read = true;
			new_wifi_update = true;
		}
			

		if (count % 10 == 0)					//10s interval SD save + alarm check
		{
			new_SD_save = true;
			
			if (alarm_on)
			{
				//short_beep(true, 1, 0);
				//alarm_on = false;				//should turn off somewhere else + log?
			}
		}
		
	}
}
