/**
******************************************************************************
* File Name          : gpio.h
* Description        : General purpose IO functions
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"


/* Functions ------------------------------------------------------------------*/

extern void MX_GPIO_Init(void);