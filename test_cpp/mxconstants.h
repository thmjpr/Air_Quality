/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MXCONSTANT_H
#define __MXCONSTANT_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define Xtal_Osc_In_Pin GPIO_PIN_0
#define Xtal_Osc_In_GPIO_Port GPIOH
#define Xtal_Osc_Out_Pin GPIO_PIN_1
#define Xtal_Osc_Out_GPIO_Port GPIOH
#define LCD_D_C_Pin GPIO_PIN_0
#define LCD_D_C_GPIO_Port GPIOC
#define LCD_CS_Pin GPIO_PIN_1
#define LCD_CS_GPIO_Port GPIOC
#define LCD_Reset_Pin GPIO_PIN_2
#define LCD_Reset_GPIO_Port GPIOC
#define LCD_MOSI_Pin GPIO_PIN_3
#define LCD_MOSI_GPIO_Port GPIOC
#define PWM_Heater_Pin GPIO_PIN_0
#define PWM_Heater_GPIO_Port GPIOA
#define PM_Tx_Pin GPIO_PIN_2
#define PM_Tx_GPIO_Port GPIOA
#define PM_Rx_Pin GPIO_PIN_3
#define PM_Rx_GPIO_Port GPIOA
#define Green_LED_Pin GPIO_PIN_5
#define Green_LED_GPIO_Port GPIOA
#define CO2_Tx_Pin GPIO_PIN_4
#define CO2_Tx_GPIO_Port GPIOC
#define CO2_Rx_Pin GPIO_PIN_5
#define CO2_Rx_GPIO_Port GPIOC
#define ADC1_CO_Pin GPIO_PIN_0
#define ADC1_CO_GPIO_Port GPIOB
#define ADC1_IN16_Pin GPIO_PIN_1
#define ADC1_IN16_GPIO_Port GPIOB
#define LCD_Clk_Pin GPIO_PIN_10
#define LCD_Clk_GPIO_Port GPIOB
#define Button_left_Pin GPIO_PIN_11
#define Button_left_GPIO_Port GPIOB
#define Button_OK_Pin GPIO_PIN_12
#define Button_OK_GPIO_Port GPIOB
#define Pwr_3V_en_Pin GPIO_PIN_13
#define Pwr_3V_en_GPIO_Port GPIOB
#define SPI_MI_Spare_Pin GPIO_PIN_14
#define SPI_MI_Spare_GPIO_Port GPIOB
#define CO_En_Pin GPIO_PIN_15
#define CO_En_GPIO_Port GPIOB
#define SD1_CS_Pin GPIO_PIN_7
#define SD1_CS_GPIO_Port GPIOC
#define SD1_D0_Pin GPIO_PIN_8
#define SD1_D0_GPIO_Port GPIOC
#define Pwr_5V_En_Pin GPIO_PIN_9
#define Pwr_5V_En_GPIO_Port GPIOC
#define WiFi_Shutdown_Pin GPIO_PIN_8
#define WiFi_Shutdown_GPIO_Port GPIOA
#define WiFi_Tx_Pin GPIO_PIN_9
#define WiFi_Tx_GPIO_Port GPIOA
#define WiFi_Rx_Pin GPIO_PIN_10
#define WiFi_Rx_GPIO_Port GPIOA
#define USB_DM_Pin GPIO_PIN_11
#define USB_DM_GPIO_Port GPIOA
#define USB_DP_Pin GPIO_PIN_12
#define USB_DP_GPIO_Port GPIOA
#define DBG_SWDIO_Pin GPIO_PIN_13
#define DBG_SWDIO_GPIO_Port GPIOA
#define DBG_SWClk_Pin GPIO_PIN_14
#define DBG_SWClk_GPIO_Port GPIOA
#define SD1_CLK_Pin GPIO_PIN_12
#define SD1_CLK_GPIO_Port GPIOC
#define SD1_CMD_Pin GPIO_PIN_2
#define SD1_CMD_GPIO_Port GPIOD
#define SWO_Serial_out_Pin GPIO_PIN_3
#define SWO_Serial_out_GPIO_Port GPIOB
#define Piezo_TIM16_Pin GPIO_PIN_8
#define Piezo_TIM16_GPIO_Port GPIOB
#define Button_right_Pin GPIO_PIN_9
#define Button_right_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MXCONSTANT_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
