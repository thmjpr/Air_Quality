//----------------------------------------------------------------------------
//  Description:  This file contains definitions specific to the LMP91000.
//  All the LMP91000 register addresses as well as some common masks for these registers 
//  are defined. 
//
//
//  Thomas Price
//	Texas Instruments (Vishy Natarajan)
//   
//------------------------------------------------------------------------------
// Change Log:
//------------------------------------------------------------------------------
// Version:  1.00
//			Initial Release Version
// Thomas - Convert to c++, add register definitions, add functions to set registers, etc.
//		  - Note that some registers are not specified on LMP91002 but seem to work OK
//------------------------------------------------------------------------------
#ifndef HEADER_FILE_LMP91000_H
#define HEADER_FILE_LMP91000_H

class Lmp91000
{
public:
	 /**
	 * @brief   Transimpedance amplifier control register (TIACN)
	 * @details Controls the transimpedance gain (various internal or external).
	 */
	typedef enum
	{
		TIA_GAIN_EXT  = 0 << 2,	//Gain set by external resistor, 350K maximum
		TIA_GAIN_2K75 = 1 << 2,
		TIA_GAIN_3K5  = 2 << 2,
		TIA_GAIN_7K   = 3 << 2,
		TIA_GAIN_14K  = 4 << 2,
		TIA_GAIN_35K  = 5 << 2,
		TIA_GAIN_120K = 6 << 2,
		TIA_GAIN_350K = 7 << 2,
	}TIACN;

	 /**
	 * @brief   RLOAD resistance control for use in lmp91000
	 * @details 
	 */
	typedef enum
	{
		RLOAD_10  = 0 << 0,
		RLOAD_33  = 1 << 0,
		RLOAD_50  = 2 << 0,
		RLOAD_100 = 3 << 0,
	}RLOAD;
	
	 /**
	 * @brief   Reference control register (REFCN)
	 * @details Controls the reference voltage (vref or vdd)
	 */
	typedef enum
	{
		REFCN_INT    = 0 << 7,		//Interal	(Vdd)
		REFCN_EXT    = 1 << 7,		//External (Vref)
		INT_ZERO_20  = 0 << 5,		//0%
		INT_ZERO_50  = 1 << 5,		//default
		INT_ZERO_67  = 2 << 5,		//
		INT_ZERO_BYP = 3 << 5,		//For O2 sensor only
		BIAS_NEG     = 0 << 4,		//Bias polarity negative (default)
		BIAS_POS     = 1 << 4,		//Bias polarity positive
		DIAG_0       = 0 << 0,		//0% bias (default)
		DIAG_1       = 1 << 0,		//1% bias of source reference (for testing)
	}REFCN;
	
	/**
	 * @brief   Mode control register
	 * @details Controls the operation modes
	 */
	typedef enum
	{
		FET_SHORT_N      = 0 << 7,		//FET not shorted (default)
		FET_SHORT_Y      = 1 << 7,		//FET shorting sensor (save power)
		OP_MODE_SLP      = 0 << 0,		//Sleep
		OP_MODE_2_LEAD   = 1 << 0,		//
		OP_MODE_STDBY    = 2 << 0,		//Standby
		OP_MODE_3LEAD    = 3 << 0,		//Run mode (3-lead sensor)
	
		OP_MODE_TEMP     = 6 << 0,
		OP_MODE_TEMP_TIA = 7 << 0,
	}MODECN;
 
	/**
	 * Lmp91000 constructor.
	 *
	 */
	Lmp91000();

	/**
	 * Lmp91000 destructor.
	 */
	~Lmp91000();

	  /**
	 * @brief   Reset.
	 * @details Sends the rest command.
	 * @returns 0 if no errors, -1 if error.
	 */
	int reset(void);
	
	/**
	 * @brief   Initialization.
	 * @details 
	 * @returns 0 if no errors, -1 if error.
	 */
	int init(void);

	 /**
	 * @brief   Configure transimpedance gain
	 * @details 
	 * @returns 0 if no errors, -1 if error.
	 */
	int configGain(Lmp91000::TIACN gain);
	
	int configReference(Lmp91000::REFCN source, REFCN zero);
	
	int configMode(Lmp91000::MODECN mode, Lmp91000::MODECN mode2 = FET_SHORT_N);
	int enableDiagnostic(bool enable);

	int lock();
	
	int unlock();
	
	bool isReady();
 
private:
	uint8_t lmp_read_reg(uint8_t reg);
	int lmp_write_reg(uint8_t reg, uint8_t data);
	//uint8_t crc8(uint8_t value, uint8_t seed);
	//bool i2c_owner;
};















#endif                                                        // HEADER_FILE_LMP91000_H

