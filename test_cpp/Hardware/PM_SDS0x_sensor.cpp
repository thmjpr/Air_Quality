/**
  ******************************************************************************
  * @file    SDS_PM_sensor.cpp
  * @author  Thomas
  * @version 
  * @date    Aug 11 2016
  * @brief   
  ******************************************************************************
  * @attention
  * PM sensor will default to output data every 1 second. Use the Nova sensor software
  * to change to passive mode to allow manual requests.
  * Tested with SDS021, SDS011
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "PM_SDS0x_sensor.h"
#include "mxconstants.h"
#include "error.h"


//******************************************************************************
SDS_PM::SDS_PM(UART_HandleTypeDef * uart_ptr)
{
	uart = uart_ptr;
	present = false;
}
 

//******************************************************************************
SDS_PM::~SDS_PM()
{
}

//******************************************************************************
bool SDS_PM::is_present()
{
	return present;
}

//-----------------------------------------------------------------------------
/**
  * @brief  Power on the 5V supply to external PM sensor
  * @param  
  * @retval  
  */
void SDS_PM::power(bool power)
{
	if (power)
		HAL_GPIO_WritePin(Pwr_5V_En_GPIO_Port, Pwr_5V_En_Pin, GPIO_PIN_SET);

	else
		HAL_GPIO_WritePin(Pwr_5V_En_GPIO_Port, Pwr_5V_En_Pin, GPIO_PIN_RESET);
}

//******************************************************************************
/**
  * @brief  Initialize the PM sensor (power it up and check its present)
  * @param  
  * @retval  
  * @details Blocking process which will take about 3 seconds
  */

bool SDS_PM::init(void)
{
	uint32_t pm10;
	
	power(true);
	HAL_Delay(2500);					//Delay for PM sensor to bootup and transmit entry command

	if (read_blocking(&pm10))
		present = true;
	else
		present = false;
	
	return present;
}

//-----------------------------------------------------------------------------
void SDS_PM::request(void)
{
	HAL_StatusTypeDef state;
	
	//Setup a UART DMA receive first to receive packet
	//if (HAL_UART_Receive_DMA(uart, (uint8_t *)RxBuffer, sizeof(RxBuffer)) != HAL_OK)
	state = HAL_UART_Receive_DMA(uart,	(uint8_t *)RxBuffer,sizeof(RxBuffer));
	if(state != HAL_OK)
		Error_Handler();
	
	//Then transmit request for new PM data
	if (HAL_UART_Transmit_DMA(uart, (uint8_t*)REQ_PM_Packet, sizeof(REQ_PM_Packet)) != HAL_OK)
		Error_Handler();
}

//-----------------------------------------------------------------------------

bool SDS_PM::read_blocking(uint32_t * pm10)
{
	HAL_StatusTypeDef response;
	
	response  = HAL_UART_Transmit(uart, (uint8_t*)REQ_PM_Packet, sizeof(REQ_PM_Packet), 50);		//request for PM reading	

	if (HAL_UART_Receive(uart, (uint8_t *)RxBuffer, sizeof(RxBuffer), 300) != HAL_OK)				//receive reading
		return false;

	*pm10 = static_cast<int>(get_pm10_level());
	
	return true;
}

//-----------------------------------------------------------------------------

float SDS_PM::get_pm25_level()
{
	int level;
	
	if ((RxBuffer[0] == SDS_MSG_HEAD) & (RxBuffer[8] == checksum(RxBuffer)))
	{
		level = RxBuffer [3] * 256 + RxBuffer[2];
		return level / 10.0;			//divide by 10 for actual value
	}
	
	else
	{
		return -1;				//error condition
	}
}


//-----------------------------------------------------------------------------

float SDS_PM::get_pm10_level()
{
	int level;
	
	if ((RxBuffer[0] == SDS_MSG_HEAD) & (RxBuffer[8] == checksum(RxBuffer)))
	{
		level = RxBuffer[5] * 256 + RxBuffer[4];
		return level / 10.0;			//divide by 10 for actual value
	}
	
	else
	{
		return -1;				//error condition
	}
}

float SDS_PM::get_pm1_level()
{
	return -1;
}

//-----------------------------------------------------------------------------
/**
  * @brief		Checksum
  * @param		None
  * @retval		None
  */

uint8_t SDS_PM::checksum(uint8_t* packet)
{
	uint8_t i;
	uint8_t checksum = 0;
	
	for (i = 2; i <= 7; i++)			//checksum covers DATA1 to DATA6
	{
		checksum += packet[i];
	}
	
	return checksum;
}

//-----------------------------------------------------------------------------
/**
  * @brief  
  * @param  None
  * @retval  None
  */

