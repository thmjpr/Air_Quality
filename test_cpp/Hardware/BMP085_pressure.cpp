/**
  ******************************************************************************
  * @file    BMP085.c
  * @author  Thomas
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  * Read call may take 5-26ms!
  *
  * Spec at 25C
  * Temperature:	+/- 1.5 C
  * Pressure:		+/- 20 Pa
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "BMP085_pressure.hpp"
#include "mxconstants.h"
#include "math.h"
#include "i2c.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/


/* Private functions ---------------------------------------------------------*/



/*  Copyright (c) 2010  Christoph Mair <christoph.mair@gmail.com>
 *  Copyright (c) 2012  Bosch Sensortec GmbH
 *  Copyright (c) 2012  Unixphere AB
 *
 *  This driver supports the bmp085 and bmp18x digital barometric pressure and temperature sensors from Bosch Sensortec. The datasheets are available from their website:
 *  http://www.bosch-sensortec.com/content/language1/downloads/BST-BMP085-DS000-05.pdf
 *  http://www.bosch-sensortec.com/content/language1/downloads/BST-BMP180-DS000-07.pdf
 *
 *  A pressure measurement is issued by reading from pressure0_input. The return value ranges from 30000 to 110000 pascal with a resulution of 1 pascal (0.01 millibar) which enables measurements from 9000m above
 *  to 500m below sea level.
 *
 *  The temperature can be read from temp0_input. Values range from -400 to 850 representing the ambient temperature in degree celsius multiplied by 10.The resolution is 0.1 celsius.
 *
 *  Because ambient pressure is temperature dependent, a temperature measurement will be executed automatically even if the user is reading from pressure0_input. This happens if the last temperature measurement has been executed more then one second ago.
 *
 *  To decrease RMS noise from pressure measurements, the bmp085 can autonomously calculate the average of up to eight samples. This is set up by writing to the oversampling sysfs file. Accepted values are 0, 1, 2 and 3. 2^x when x is the value written to this file specifies the number of samples used to calculate the ambient pressure. RMS noise is specified with six pascal (without averaging) and decreases down to 3 pascal when using an oversampling setting of 3.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 */

/*************************************************** 
  This is a library for the Adafruit BMP085/BMP180 Barometric Pressure + Temp sensor

  Designed specifically to work with the Adafruit BMP085 or BMP180 Breakout 
  ----> http://www.adafruit.com/products/391
  ----> http://www.adafruit.com/products/1603

  These displays use I2C to communicate, 2 pins are required to  
  interface
  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include "bmp085_pressure.hpp"
#include "stdbool.h"
#include "delay.h"

BMP085::BMP085() {
}


bool BMP085::begin(uint8_t mode) {
	if (mode > BMP085_ULTRAHIGHRES) 
		mode = BMP085_ULTRAHIGHRES;
	oversampling = mode;

	//initiate I2C //wire();

	if (read8(0xD0) != 0x55) {
		present = false;
		return false;
	}

	  /* read calibration data */
	ac1 = read16(BMP085_CAL_AC1);
	ac2 = read16(BMP085_CAL_AC2);
	ac3 = read16(BMP085_CAL_AC3);
	ac4 = read16(BMP085_CAL_AC4);
	ac5 = read16(BMP085_CAL_AC5);
	ac6 = read16(BMP085_CAL_AC6);

	b1 = read16(BMP085_CAL_B1);
	b2 = read16(BMP085_CAL_B2);

	mb = read16(BMP085_CAL_MB);
	mc = read16(BMP085_CAL_MC);
	md = read16(BMP085_CAL_MD);
	
#if (BMP085_DEBUG == 1)
	Serial.print("ac1 = "); Serial.println(ac1, DEC);
	Serial.print("ac2 = "); Serial.println(ac2, DEC);
	Serial.print("ac3 = "); Serial.println(ac3, DEC);
	Serial.print("ac4 = "); Serial.println(ac4, DEC);
	Serial.print("ac5 = "); Serial.println(ac5, DEC);
	Serial.print("ac6 = "); Serial.println(ac6, DEC);

	Serial.print("b1 = "); Serial.println(b1, DEC);
	Serial.print("b2 = "); Serial.println(b2, DEC);

	Serial.print("mb = "); Serial.println(mb, DEC);
	Serial.print("mc = "); Serial.println(mc, DEC);
	Serial.print("md = "); Serial.println(md, DEC);
#endif
	present = true;
	return true;
}

int32_t BMP085::computeB5(int32_t UT) {
	int32_t X1 = (UT - (int32_t)ac6) * ((int32_t)ac5) >> 15;
	int32_t X2 = ((int32_t)mc << 11) / (X1 + (int32_t)md);
	return X1 + X2;
}

uint16_t BMP085::readRawTemperature(void) {
	write8(BMP085_CONTROL, BMP085_READTEMPCMD);
	delay_ms(5);	
	
#if BMP085_DEBUG == 1
	Serial.print("Raw temp: "); Serial.println(read16(BMP085_TEMPDATA));
#endif
	
	return read16(BMP085_TEMPDATA);
}

uint32_t BMP085::readRawPressure(void) {
	uint32_t raw;

	write8(BMP085_CONTROL, BMP085_READPRESSURECMD + (oversampling << 6));

	if (oversampling == BMP085_ULTRALOWPOWER) 
		delay_ms(5);
	else if (oversampling == BMP085_STANDARD) 
		delay_ms(8);
	else if (oversampling == BMP085_HIGHRES) 
		delay_ms(14);
	else 
		delay_ms(26);

	raw = read16(BMP085_PRESSUREDATA);

	raw <<= 8;
	raw |= read8(BMP085_PRESSUREDATA + 2);
	raw >>= (8 - oversampling);

	 /* this pull broke stuff, look at it later?
	  if (oversampling==0) {
	    raw <<= 8;
	    raw |= read8(BMP085_PRESSUREDATA+2);
	    raw >>= (8 - oversampling);
	  }
	 */

#if BMP085_DEBUG == 1
	Serial.print("Raw pressure: "); Serial.println(raw);
#endif
	return raw;
}


//Return pressure in 

int32_t BMP085::readPressure(void) {
	int32_t UT, UP, B3, B5, B6, X1, X2, X3, p;
	uint32_t B4, B7;

	UT = readRawTemperature();
	UP = readRawPressure();

#if BMP085_DEBUG == 1
	  // use datasheet numbers!
	UT = 27898;
	UP = 23843;
	ac6 = 23153;
	ac5 = 32757;
	mc = -8711;
	md = 2868;
	b1 = 6190;
	b2 = 4;
	ac3 = -14383;
	ac2 = -72;
	ac1 = 408;
	ac4 = 32741;
	oversampling = 0;
#endif

	B5 = computeB5(UT);

#if BMP085_DEBUG == 1
	Serial.print("X1 = "); Serial.println(X1);
	Serial.print("X2 = "); Serial.println(X2);
	Serial.print("B5 = "); Serial.println(B5);
#endif

 // do pressure calcs
	B6 = B5 - 4000;
	X1 = ((int32_t)b2 * ((B6 * B6) >> 12)) >> 11;
	X2 = ((int32_t)ac2 * B6) >> 11;
	X3 = X1 + X2;
	B3 = ((((int32_t)ac1 * 4 + X3) << oversampling) + 2) / 4;

#if BMP085_DEBUG == 1
	Serial.print("B6 = "); Serial.println(B6);
	Serial.print("X1 = "); Serial.println(X1);
	Serial.print("X2 = "); Serial.println(X2);
	Serial.print("B3 = "); Serial.println(B3);
#endif

	X1 = ((int32_t)ac3 * B6) >> 13;
	X2 = ((int32_t)b1 * ((B6 * B6) >> 12)) >> 16;
	X3 = ((X1 + X2) + 2) >> 2;
	B4 = ((uint32_t)ac4 * (uint32_t)(X3 + 32768)) >> 15;
	B7 = ((uint32_t)UP - B3) * (uint32_t)(50000UL >> oversampling);

#if BMP085_DEBUG == 1
	Serial.print("X1 = "); Serial.println(X1);
	Serial.print("X2 = "); Serial.println(X2);
	Serial.print("B4 = "); Serial.println(B4);
	Serial.print("B7 = "); Serial.println(B7);
#endif

	if (B7 < 0x80000000) {
		p = (B7 * 2) / B4;
	}
	else {
		p = (B7 / B4) * 2;
	}
	X1 = (p >> 8) * (p >> 8);
	X1 = (X1 * 3038) >> 16;
	X2 = (-7357 * p) >> 16;

#if BMP085_DEBUG == 1
	Serial.print("p = "); Serial.println(p);
	Serial.print("X1 = "); Serial.println(X1);
	Serial.print("X2 = "); Serial.println(X2);
#endif

	p = p + ((X1 + X2 + (int32_t)3791) >> 4);
	
#if BMP085_DEBUG == 1
	Serial.print("p = "); Serial.println(p);
#endif
	return p;
}

int32_t BMP085::readSealevelPressure(float altitude_meters) {
	float pressure = readPressure();
	return (int32_t)(pressure / pow(1.0 - altitude_meters / 44330, 5.255));
}

float BMP085::readTemperature(void) {
	int32_t UT, B5;     // following ds convention
	float temp;

	UT = readRawTemperature();

#if BMP085_DEBUG == 1
	  // use datasheet numbers!
	UT = 27898;
	ac6 = 23153;
	ac5 = 32757;
	mc = -8711;
	md = 2868;
#endif

	B5 = computeB5(UT);
	temp = (B5 + 8) >> 4;
	temp /= 10;
  
	return temp;
}

float BMP085::readAltitude(float sealevelPressure) {
	float altitude;

	float pressure = readPressure();

	altitude = 44330 * (1.0 - pow(pressure / sealevelPressure, 0.1903));

	return altitude;
}


/*********************************************************************/

uint8_t BMP085::read8(uint8_t a) {
	uint8_t ret;
	i2c_transmit_single(BMP085_I2CADDR, a);
	ret = i2c_receive_single(BMP085_I2CADDR);
	
/*
	Wire.beginTransmission(BMP085_I2CADDR); // start transmission to device 
	Wire.write(a); // sends register address to read from
	Wire.endTransmission(); // end transmission
  
	Wire.beginTransmission(BMP085_I2CADDR); // start transmission to device 
	Wire.requestFrom(BMP085_I2CADDR, 1);// send data n-bytes read
	ret = Wire.read(); // receive DATA
	Wire.endTransmission(); // end transmission
*/
	return ret;
}

uint16_t BMP085::read16(uint8_t a) {
	uint16_t ret;
	
	i2c_transmit_single(BMP085_I2CADDR, a);
	//ret = ((uint16_t)i2c_receive(BMP085_I2CADDR)) << 8;
	//i2c_transmit_single(BMP085_I2CADDR, a);
	ret = ((uint16_t)i2c_receive_single(BMP085_I2CADDR)) << 8;
	ret |= i2c_receive_single(BMP085_I2CADDR);
/*
	Wire.beginTransmission(BMP085_I2CADDR); // start transmission to device 
	Wire.write(a); // sends register address to read from
	Wire.endTransmission(); // end transmission
  
	Wire.beginTransmission(BMP085_I2CADDR); // start transmission to device 
	Wire.requestFrom(BMP085_I2CADDR, 2);// send data n-bytes read

	ret = Wire.read(); // receive DATA
	ret <<= 8;
	ret |= Wire.read(); // receive DATA
	Wire.endTransmission(); // end transmission
*/
	return ret;
}

void BMP085::write8(uint8_t a, uint8_t d) {
	
	uint8_t buf[2] = { a, d };
	
	i2c_transmit(BMP085_I2CADDR, buf, 2);
	/*
	Wire.beginTransmission(BMP085_I2CADDR); // start transmission to device 
	Wire.write(a); // sends register address to read from
	Wire.write(d);  // write data
	Wire.endTransmission(); // end transmission
	*/
}



//-----------------------------------------------------------------------------
/**
  * @brief  
  * @param  
  * @retval  
  */

