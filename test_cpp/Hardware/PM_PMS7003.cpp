/**
  ******************************************************************************
  * @file    PM_PMS7003.cpp
  * @author  Thomas
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  * 
  * 
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "PM_PMS7003.h"
#include "mxconstants.h"
#include "error.h"
#include "usart.h"


//******************************************************************************
PM_PLANT::PM_PLANT(UART_HandleTypeDef * uart_ptr)
{
	uart = uart_ptr;
	present = false;
}
 

//******************************************************************************
PM_PLANT::~PM_PLANT()
{
}

//******************************************************************************
bool PM_PLANT::is_present()
{
	return present;
}

//-----------------------------------------------------------------------------
/**
  * @brief  Power on the 5V supply to external PM sensor
  * @param  
  * @retval  
  */
void PM_PLANT::power(bool power)
{
	if (power)
		HAL_GPIO_WritePin(Pwr_5V_En_GPIO_Port, Pwr_5V_En_Pin, GPIO_PIN_SET);

	else
		HAL_GPIO_WritePin(Pwr_5V_En_GPIO_Port, Pwr_5V_En_Pin, GPIO_PIN_RESET);
}

//******************************************************************************
/**
  * @brief  Initialize the PM sensor (power it up and check its present)
  * @param  
  * @retval  
  * @details Blocking process which will take about 3 seconds
  */

bool PM_PLANT::init(void)
{
	uint32_t pm10;
	
	power(true);
	
	if (read_blocking(&pm10))
		present = true;
	else
		present = false;
	
	//Setup the next receive
	HAL_StatusTypeDef state;
	state = HAL_UART_Receive_DMA(uart, (uint8_t *)RxBuffer, sizeof(RxBuffer));
	if (state != HAL_OK)
			Error_Handler();	
	
	return present;
}

//-----------------------------------------------------------------------------
void PM_PLANT::request(void)
{
	HAL_StatusTypeDef state;
	
	//Setup a UART DMA receive to receive packet
	
	//if already waiting then dont set up another DMA somehow?
	
	//If receive is complete, set up another
	if (Uart2_Rx_Ready)
	{
		load_packet();		//decode the last packet
		
		state = HAL_UART_Receive_DMA(uart, (uint8_t *)RxBuffer, sizeof(RxBuffer));
		if (state != HAL_OK)
			Error_Handler();	
	}
	
	else
	{

	}
}

//-----------------------------------------------------------------------------

bool PM_PLANT::read_blocking(uint32_t * pm10)
{	
	uint32_t i;
	
	//Wait for start byte to be received
	for (i = 0; i < 32; i++)
	{		
		HAL_UART_Receive(uart, (uint8_t *)RxBuffer, 1, 100);
		if (RxBuffer[0] == MSG_START1)
			break;
	}
	
	//Receive rest of packet
	if (HAL_UART_Receive(uart, (uint8_t *)RxBuffer + 1, sizeof(RxBuffer) - 1, 5000) != HAL_OK)		//receive packet
		return false;

	load_packet();
	*pm10 = static_cast<int>(get_pm10_level());
	
	return verify_crc();
}

//-----------------------------------------------------------------------------
// Could also do some memcpy, etc.
void PM_PLANT::load_packet()
{
	rx.start =			(RxBuffer[0] << 8) + RxBuffer[1];
	rx.frame_length =	(RxBuffer[2] << 8) + RxBuffer[3];
	rx.pm1_conc =		(RxBuffer[4] << 8) + RxBuffer[5];
	rx.pm2_conc =		(RxBuffer[6] << 8) + RxBuffer[7];
	rx.pm10_conc =		(RxBuffer[8] << 8) + RxBuffer[9];
	rx.pm1_conc_atm =	(RxBuffer[10] << 8) + RxBuffer[11];
	rx.pm2_conc_atm =	(RxBuffer[12] << 8) + RxBuffer[13];
	rx.pm10_conc_atm =	(RxBuffer[14] << 8) + RxBuffer[15];
	rx.count_0_3_um =	(RxBuffer[16] << 8) + RxBuffer[17];
	rx.count_0_5_um =	(RxBuffer[18] << 8) + RxBuffer[19];
	rx.count_1_0_um =	(RxBuffer[20] << 8) + RxBuffer[21];
	rx.count_2_5_um =	(RxBuffer[22] << 8) + RxBuffer[23];
	rx.count_5_0_um =	(RxBuffer[24] << 8) + RxBuffer[25];
	rx.count_10_um =	(RxBuffer[26] << 8) + RxBuffer[27];
	rx.fw =				RxBuffer[28];
	rx.error =			RxBuffer[29];
	rx.crc =			(RxBuffer[30] << 8) + RxBuffer[31];
	
	if (verify_crc() == false)
	{
		rx = (const struct PMS7003_packet) { 0 };
	}
		
}


//-----------------------------------------------------------------------------
bool PM_PLANT::verify_crc()
{
	//we dont need to check CRC every time.
	if (rx.crc == checksum(RxBuffer))
		return true;
	else
		return false;
}


//-----------------------------------------------------------------------------
float PM_PLANT::get_pm25_level()
{
	return rx.pm2_conc_atm;
}

//-----------------------------------------------------------------------------
float PM_PLANT::get_pm10_level()
{
	return rx.pm10_conc_atm;
}

//-----------------------------------------------------------------------------
float PM_PLANT::get_pm1_level()
{
	return rx.pm1_conc_atm;
}


//-----------------------------------------------------------------------------
/**
  * @brief		Checksum
  * @param		None
  * @retval		None
  */

uint16_t PM_PLANT::checksum(volatile uint8_t* packet)
{
	uint16_t i;
	uint16_t checksum = 0;
	
	for (i = 0; i < TX_PACKET_LEN - 2; i++)			//checksum covers Start 1 to Data 13 low
	{
		checksum += packet[i];
	}
	
	return checksum;
}
