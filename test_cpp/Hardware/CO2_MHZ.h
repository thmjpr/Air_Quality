/**
  ******************************************************************************
  * @file    CO2_MHZ.h
  * @author  
  * @version 
  * @date    Aug 6 2016
  * @brief   Interface code for Winsen sensors MH-Z19 CO2 sensor module.
  ******************************************************************************
  * Baud: 9600, 8byte, 1 stop bit
  * Command list:
  * - 0x86 Gas concentration
  * - 0x87 Calibrate zero point
  * - 0x88 Calibrate span point
  *
  * Packet
  * Start	Sensor #	Command									Check value
  * 0xFF	0x01		0x86		0x00 0x00 0x00 0x00 0x00	0x79
  *
  * Return
  * Start	Command		High	Low		-	-	-	-	Check value
  * 0xFF	0x86		0x02	0x60	0x47 0x00 0x00	0xD1
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Exported constants --------------------------------------------------------*/
#define CO2_RANGE	2000		//ppm range of sensor, other ranges are available
#define CO2_PACKET	9


/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "stdbool.h"
#include "stdlib.h"

	class CO2_MHZ
	{
	public:
		CO2_MHZ(UART_HandleTypeDef * uart_ptr);
		~CO2_MHZ();
	
		/**
		  * @brief  Initialize the CO2 sensor (power it up and check its present)
		  * @param  
		  * @retval  
		  * @details Blocking process which will take about 500ms
		  */
		bool init(void);

		/**
		* @brief  Power on the 5V supply to the CO2 sensor.
		* @param  
		* @retval  
		* @details Be aware of power on requirements, takes special power up procedure or alternatively can wait a few seconds.
		*/
		void power(bool power);
		
		/**
		* @brief  Run this every ~5s to send request for co2 data
		* @param  none
		* @retval  none
		*/
		void request(void);
	
		/**
		  * @brief  After waiting for CO2 data and receiving it, call this to retrieve ppm level of CO2
		  * @param  None
		  * @retval  CO2 level in PPM
		  */
		uint32_t get_co2_level();

		/**
		  * @brief	Transmit read request ot CO2 sensor and wait for response (blocking)
		  * @param	Pointer to CO2
		  * @retval  If reading was successful
		  * @note	
		  */
		bool read_blocking(uint32_t * co2);
		
		bool present = false;
	
	private:
		/**
		* @brief		Checksum for MH-Z19 sensor
		* @param		CO2 sensor message packet.
		* @retval		Checksum
		*/
		uint8_t mh_checksum(uint8_t* packet);

		UART_HandleTypeDef * uart;
		
		uint8_t RxBuffer[CO2_PACKET];
		uint8_t TxBuffer[CO2_PACKET];	//hm
	};

/*-------------------------------END OF FILE-------------------------------*/




