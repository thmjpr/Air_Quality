/**
  ******************************************************************************
  * @file    i2c.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * 
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

	 
/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

extern void MX_I2C1_Init(void);
bool i2c_transmit_single(uint8_t address, uint8_t data);
uint8_t i2c_transmit(uint8_t address, uint8_t * data, uint8_t data_len);
uint8_t i2c_receive_single(uint8_t address);
uint8_t i2c_receive(uint8_t address, uint8_t * data, uint8_t data_len);


/*-------------------------------END OF FILE-------------------------------*/




