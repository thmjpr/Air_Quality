/**
  ******************************************************************************
  * @file    PM_PMS7003.h
  * @author  Thomas
  * @version 
  * @date    
  * @brief   Interface code for Plantower PMS7003 particulate matter sensor (PM1.0/PM2.5/PM10)
  ******************************************************************************
  * Baud: 9600, 8byte, 1 stop bit
  * 
  * Sensor will transmit a packet every 1s?
  * Sensor reading packet (see structure below): 
  * 0x42 0x4d len pm_cf1 ... pmcfa ... pm_cnt ... fw ... error ... crc 
  * 
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "stdbool.h"
#include "stdint.h"

typedef struct PMS7003_packet
{
	uint16_t start;			//{ 0x42, 0x4d };
	uint16_t frame_length;	//13*2+2 = 28
	//Particulate concentrations in ug/m3. Standard particles (correction factor CF=1)
	uint16_t pm1_conc;
	uint16_t pm2_conc;
	uint16_t pm10_conc;
	//Particulate concentrations in ug/m3. Atmospheric (real world level)
	uint16_t pm1_conc_atm;
	uint16_t pm2_conc_atm;
	uint16_t pm10_conc_atm;
	//Particulate counts for specific particle sizes
	uint16_t count_0_3_um;	//0.3um
	uint16_t count_0_5_um;	//0.5um
	uint16_t count_1_0_um;	//1.0um
	uint16_t count_2_5_um;	//2.5um
	uint16_t count_5_0_um;	//5.0um
	uint16_t count_10_um;	//10um
	uint8_t fw;				//
	uint8_t error;			//
	uint16_t crc;			//CRC = start1 + start2 + ... data 13 high + data 13 low
}PMS7003_packet;


/* Functions -----------------------------------------------------------------*/

class PM_PLANT
{
	static const uint8_t TX_PACKET_LEN = 32;		//
	static const uint8_t RX_PACKET_LEN = 32;		//

	const uint8_t MSG_START1 = 0x42;
	const uint8_t MSG_START2 = 0x4d;

public:
	const float PM_RANGE = 500.0;					//Range of sensor (ug/m3)

	PM_PLANT(UART_HandleTypeDef * uart_ptr);
	~PM_PLANT();
	bool is_present();
	
	/**
	* @brief Initialzation of particulate sensor
	* @param  
	* @retval  
	*/
	bool init(void);
	
	/**
	* @brief Power on particulate sensor (5V)
	* @param  
	* @retval  
	* @note	
	*/
	void power(bool power);

	/**
	* @brief Perform blocking read of PM10 level
	* @param  
	* @retval  PM10 level (ug/m^3 concentration)
	* @note Can take ~10-60ms to respond
	*/
	bool read_blocking(uint32_t * pm10);
	
	/**
	* @brief  Run this every ~1s to send request for PM data
	* @param  
	* @retval  
	*/
	void request(void);
	
	/**
  * @brief  Retrieve PM10 level
  * @param  None
  * @retval PM1.0 level (ug/m^3 concentration)
  */
	float get_pm1_level();
	
	/**
  * @brief  Retrieve PM10 level
  * @param  None
  * @retval PM2.5 level (ug/m^3 concentration)
  */
	float get_pm25_level();
	
	/**
  * @brief  Retrieve PM2.5 level
  * @param  None
  * @retval PM10 level (ug/m^3 concentration)
  */
	float get_pm10_level();
	


private:
	/**
	* @brief 
	* @param  
	* @retval  
	*/
	uint16_t checksum(volatile uint8_t* packet);
	
	/**
	* @brief 
	* @param  
	* @retval  
	*/
	bool verify_crc();
	
	/**
	* @brief 
	* @param  
	* @retval  
	*/
	void load_packet(void);
	
	bool present;
	UART_HandleTypeDef * uart;

	volatile uint8_t TxBuffer[TX_PACKET_LEN] = { };
	volatile uint8_t RxBuffer[RX_PACKET_LEN];
	
	PMS7003_packet rx;
};


/*-------------------------------END OF FILE-------------------------------*/