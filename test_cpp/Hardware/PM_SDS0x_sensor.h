/**
  ******************************************************************************
  * @file    SDS_PM_sensor.h
  * @author  Thomas
  * @version 
  * @date    Aug 11 2016
  * @brief   Interface code for Nova Fitness SDS011/SDS021 particulate matter sensor (PM2.5/PM10)
  ******************************************************************************
  * Baud: 9600, 8byte, 1 stop bit
  *
  * Packet to sensor
  * http://www.gu-wen.com/Faq/402.html								
  *	Start	Cmd		Data1														Sensor ID (FF = ALL)	ID		Check	Tail
  * 0xAA	0xB4	0x04	0x	0x	0x	0x	0x	0x	0x	0x	0x	0x	0x	0x	0x	0xFF					0xFF	0x		0xAB
  *
  * Sensor reading packet
  * Start	Command		PM2.5L	PM2.5H	PM10L	PM10H	ID1	ID2	Check	Tail
  * 0xAA	0xC0		0x		0x		0x		0x		0x	0x	0x		0xAB
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

//Sensor is constantly sending unless you change the configuration to passive mode
//Can be done on PC using Nova software

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "stdbool.h"
#include "stdint.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

class SDS_PM
{
	static const uint8_t TX_PACKET_LEN = 19;		//
	static const uint8_t RX_PACKET_LEN = 10;		//

	const uint8_t SDS_MSG_HEAD = 0xAA;
	const uint8_t SDS_MSG_TAIL = 0xAB;
	const uint8_t REQ_PM_Packet[TX_PACKET_LEN] = { SDS_MSG_HEAD, 0xB4, 0x04, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 0x02, SDS_MSG_TAIL };

public:
	const float PM_RANGE = 999.9;					//Range of sensor (ug/m3)

	SDS_PM(UART_HandleTypeDef * uart_ptr);
	~SDS_PM();
	bool is_present();
	
	/**
	* @brief Initialzation of particulate sensor
	* @param  
	* @retval  
	*/
	bool init(void);
	
	/**
	* @brief Power on particulate sensor (5V)
	* @param  
	* @retval  
	* @note	
	*/
	void power(bool power);

	/**
	* @brief Perform blocking read of PM10 level
	* @param  
	* @retval  PM10 level (ug/m^3 concentration)
	* @note Can take ~10-60ms to respond
	*/
	bool read_blocking(uint32_t * pm10);
	
	/**
	* @brief  Run this every ~1s to send request for PM data
	* @param  
	* @retval  
	*/
	void request(void);
	
	/**
  * @brief  Retrieve PM2.5 level
  * @param  None
  * @retval PM25 level (ug/m^3 concentration)
  */
	float get_pm25_level();
	
  /**
  * @brief  Retrieve PM10 level
  * @param  None
  * @retval PM10 level (ug/m^3 concentration)
  */
	float get_pm10_level();

	float get_pm1_level();
	
private:
	uint8_t checksum(uint8_t* packet);
	bool present;
	UART_HandleTypeDef * uart;

	uint8_t TxBuffer[TX_PACKET_LEN] = {};
	uint8_t RxBuffer[RX_PACKET_LEN];
};


/*-------------------------------END OF FILE-------------------------------*/