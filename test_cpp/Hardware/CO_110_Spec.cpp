/**
  ******************************************************************************
  * @file    CO_110_Spec.c
  * @author  Thomas Price
  * @version 
  * @date    Oct 1, 2016
  * @brief   Carbon monoxide sensor, connected to AFE LMP91000
  ******************************************************************************
  * @attention
  * There is about 10min powerup time before the reading is stable
  * http://www.spec-sensors.com/wp-content/uploads/2016/05/SPEC-AN-103-Power-On-Stabilization.pdf
  * TIA gain specified as 100k
  * However, 120k/350k option might be usable.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "CO_110_Spec.h"
#include "adc.h"
#include "mxconstants.h"
#include "main.h"
#include "LMP91000.h"
#include "stdbool.h"
#include "stdlib.h"
#include "math.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define LMP_TIA_GAIN	120000		//Gain set by register or external resistor
#define CO_V_OFFSET		-0.0		//Offset adjustment
#define AMP_PER_NA		0.000001	//10^-6
#define V_PER_KV		1000
#define CO_RANGE		1000		//ppm range of sensor	
#define VOLTAGE_REF		3.300		//Voltage reference Vdda and LMP Vref

float na_per_ppm = 6.20;			//na per ppm is calibration value written on sticker of sensor
float ppm_scale;
int adc_buff[32] = { 0 };
Lmp91000 afe;


//******************************************************************************
SPEC_CO_110::SPEC_CO_110()
{
	present = false;
}

//******************************************************************************
SPEC_CO_110::~SPEC_CO_110()
{
}

//******************************************************************************
void SPEC_CO_110::power(bool power)
{
	if (power)
	{//Active low CO_ENABLE
		HAL_GPIO_WritePin(CO_En_GPIO_Port, CO_En_Pin, GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(CO_En_GPIO_Port, CO_En_Pin, GPIO_PIN_SET);
	}
}

//******************************************************************************
bool SPEC_CO_110::init(void)
{
	//Lmp91000 afe;		//local only
	power(true);
	delay_ms(1);
	
	if (!afe.isReady())
		return false;
	
	afe.unlock();
	afe.configReference(Lmp91000::REFCN_EXT, Lmp91000::INT_ZERO_20);		//20% ref
	afe.configGain(Lmp91000::TIA_GAIN_120K);								//350K gain is highest
	afe.configMode(Lmp91000::OP_MODE_3LEAD, Lmp91000::FET_SHORT_N);			//3-lead mode for CO
	
	//ISENSOR * Gain = Rtia x10^-4 A <= 3.3V
	//Sensor = 0 to 6200nA. So 0 to 2.17V
	
	//can some cal be performed with fet shorted, etc.? 

	
	//http://www.spec-sensors.com/wp-content/uploads/2016/06/User-Manual-SPEC-Sensors-SDK-17.pdf
	//ppm per volt on ADC
	ppm_scale = na_per_ppm * LMP_TIA_GAIN * AMP_PER_NA * V_PER_KV;			//Sensitivity code * TIA gain * nA * V
	
	present = true;
	
	return 0;
}

//******************************************************************************
int SPEC_CO_110::read_adc(void)
{
	uint32_t i;

	ADC_ChannelConfTypeDef sConfig;

	sConfig.Channel = ADC_CHANNEL_15;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_92CYCLES_5;			//
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
		Error_Handler();	
	
	
	HAL_ADC_Start(&hadc1);							//Start ADC conversion
	HAL_ADC_PollForConversion(&hadc1, 10);			//Wait until finished, 10ms timeout
	
	//Must perform two reads to avoid errata 2.4.4 (if reads are longer than 1ms apart).
	
	for (i = 0; i < ARR_LEN(adc_buff); i++)
	{
		HAL_ADC_Start(&hadc1);
		HAL_ADC_PollForConversion(&hadc1, 10);			//10ms timeout. Actual conversion time = 
		adc_buff[i] = HAL_ADC_GetValue(&hadc1);
	}
	
	return get_average();
}

//******************************************************************************
int SPEC_CO_110::read_blocking(float temperature)
{
	uint32_t adc, res;
	
	adc = read_adc();
	res = get_ppb(temperature, adc);
	
	return res;
}


//******************************************************************************
//Check what 0 result gives
//check for overflows..
int SPEC_CO_110::get_average(void)
{
	uint32_t avg = 0;
	
	for (int i = 0; i < ARR_LEN(adc_buff); i++)
	{
		avg += adc_buff[i];	
	}
	
	return (avg + ARR_LEN(adc_buff)/2) / ARR_LEN(adc_buff);     //round up and return average
}

//******************************************************************************
//
int SPEC_CO_110::get_ppm(float ambient_temp, uint32_t adc)
{
	float Vgas;
	float Vref = VOLTAGE_REF * 0.20;				//Reference set as 20%
	float ppm_reading;
	
	Vgas = adc / (0xFFF / VOLTAGE_REF);
	
	//ppm_reading = adc / 0xFFFF * na_per_ppm;
	ppm_reading = ppm_scale * (Vgas - Vref - CO_V_OFFSET); 
	//ppm_scale 
	
	
	//Temperature coefficient compensation (from datasheet "Temperature Effect")
	//Reduced output at lower temperatures
	if (ambient_temp < 10.0)
		ppm_reading *= 1 + (0.009 * (20 - ambient_temp));
	else		//output increases at high temperature
		ppm_reading *= 1 - (0.003 * (ambient_temp - 20));

	//Zero shift compensation
	if (ambient_temp < 0)
	{}
	else
	{
		ppm_reading -= 0.35021 * exp(0.0857637 * ambient_temp);
	}
	//else if (ambient_temp < 25)
	//	ppm_reading -= 0.4 * (ambient_temp);
	//else    //>25
	//	ppm_reading -= 1.4 * (ambient_temp - 20); 
	
	ppm_reading += 0.5;									//round up
	
	if (ppm_reading > (CO_RANGE * 2))
		ppm_reading = CO_RANGE * 2;
	else if (ppm_reading < 0)
		ppm_reading = 0;
	
	return (uint32_t)ppm_reading;
}

//******************************************************************************
//
int SPEC_CO_110::get_ppb(float ambient_temp, uint32_t adc)
{
	float Vgas;
	float Vref = VOLTAGE_REF * 0.20;				//Reference set as 20%
	float ppb_reading;
	
	Vgas = adc / (0xFFF / VOLTAGE_REF);
	ppb_reading = 1000 * ppm_scale * (Vgas - Vref - CO_V_OFFSET); 
	
	//Temperature coefficient compensation (from datasheet "Temperature Effect")
	//Reduced output at lower temperatures, so scale up
	if (ambient_temp < 10.0)
		ppb_reading *= 1 + (0.009 * (20 - ambient_temp));
	//output increases at high temperature, so scale back
	else
		ppb_reading *= 1 - (0.003 * (ambient_temp - 20));

	//Zero shift compensation
	if (ambient_temp < 0)
	{}
	else
	{
		ppb_reading -= 0.35021 * exp(0.0857637 * ambient_temp);
	}
	//else if (ambient_temp < 25)
	//	ppm_reading -= 0.4 * (ambient_temp);
	//else    //>25
	//	ppm_reading -= 1.4 * (ambient_temp - 20); 
	
	ppb_reading += 0.5;									//round up
	
	if (ppb_reading > (CO_RANGE * 2*1000))
		ppb_reading = CO_RANGE * 2*1000;
	else if (ppb_reading < 0)
		ppb_reading = 0;
	
	return (uint32_t)ppb_reading;
}

//******************************************************************************
bool SPEC_CO_110::sensor_present(void)
{
	//put REFCN to 1% then delay, then back to 0%
	afe.enableDiagnostic(true);

	HAL_Delay(1000);

	//The output should charge up and decay
	//can test health of sensor and if sensor is present

	afe.enableDiagnostic(false);
	return true;
}