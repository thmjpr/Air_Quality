/**
  ******************************************************************************
  * @file    ESP8266_wifi.c
  * @author  Thomas
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  * 
  * Can use the MAC address of the ESP8266 as a unique ID, so different devices are differentiable. 
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
//using namespace std;
#include "string.h"
#include "stm32l4xx_hal.h"
#include "ESP8266_wifi.h"
#include "stdbool.h"
#include "mxconstants.h"
#include "usart.h"


//-----------------------------------------------------------------------------
ESP8266::ESP8266(UART_HandleTypeDef * uart_ptr)
{
	uart = uart_ptr;
}
 

//-----------------------------------------------------------------------------
ESP8266::~ESP8266()
{
	uart = 0;
	present = false;
}

//-----------------------------------------------------------------------------
bool ESP8266::power(bool power)
{
	if (power)
		HAL_GPIO_WritePin(WiFi_Shutdown_GPIO_Port, WiFi_Shutdown_Pin, GPIO_PIN_SET);
		
	else
		HAL_GPIO_WritePin(WiFi_Shutdown_GPIO_Port, WiFi_Shutdown_Pin, GPIO_PIN_RESET);

	return true;
}


//-----------------------------------------------------------------------------
// Power up WiFi module and check for a response
bool ESP8266::init()
{
	uint8_t sbuf[] = "<hello_wifi>";	//Message to send
	//uint8_t h[] = "<humidity,123>";			//***FFF bug, if > missing at end it doesnt care!
	uint8_t rbuf[] = "<wifi_here>";		//expected message to get back
	int timeout = 0;
	
	present = false;
	power(true);
	HAL_Delay(3000);						//May need to wait long for bootup, dont need to wait for a connection
	
	while (timeout++ < 5)
	{
		HAL_UART_AbortReceive(uart);			//Clear out any bad data received and reset error flags
		HAL_UART_Transmit(uart, sbuf, str_len(sbuf), 50);

		//Wait for a response, 1s max
		if (HAL_UART_Receive(uart, RxBuffer, str_len(rbuf), 1000) != HAL_OK)
		{
			//
		}
		
		if (memcmp(RxBuffer, rbuf, ARR_LEN(rbuf)))						//Check expected device responded
		{
			present = true;
			return true;
		}
	}

	power(false);
	return false;
}


//-----------------------------------------------------------------------------
bool ESP8266::get_status()
{
	//return status:
	//connected, not connected, etc.
	return false;
}

//-----------------------------------------------------------------------------
// Response: <Time: 2019-02-18T09:05:35>
bool ESP8266::get_time(char * time)
{
	//get NTC time
	const char time_q[] = "NTP?";
	char buf[MAX_MSG_LEN], name[MAX_MSG_LEN];
	int i = 0;
	char * strtokIndx; // this is used by strtok() as an index
	
	send_msg(time_q);
		
	if (receive_encapsulated(buf, &i))
	{
		strtokIndx = strtok(buf, ",");      // get the first part - the string
		strcpy(name, strtokIndx);           // copy it
		strtokIndx = strtok(NULL, ",");
		strcpy(time, strtokIndx);
		return true;
	}	
	
	else
		return false;
}

//-----------------------------------------------------------------------------
bool ESP8266::get_mac()
{
	const char mac_q[] = "MAC?";
	char buf[MAX_MSG_LEN], name[MAX_MSG_LEN];
	int i = 0;
	// split the data into its parts
	char * strtokIndx; // this is used by strtok() as an index
	
	send_msg(mac_q);
	
	if (receive_encapsulated(buf, &i))
	{
		strtokIndx = strtok(buf, ",");      // get the first part - the string
		strcpy(name, strtokIndx);           // copy it
		strtokIndx = strtok(NULL, ",");
		strcpy(mac, strtokIndx);
		return true;
	}	
	
	else
		return false;
}

//-----------------------------------------------------------------------------
bool ESP8266::set_led(bool state)
{
	if (state)
		send_msg("LED_On");
	else
		send_msg("LED_Off");


	if (receive_is("LED"))
		return true;
	else
		return false;
}

//-----------------------------------------------------------------------------
bool ESP8266::send_measurements(float temperature, int humidity, int pressure, int pm1, int pm2, int pm10, int co2, int co, int o3)
{
	char sbuff[MAX_MSG_LEN] = { 0 };
	sprintf(sbuff, "temperature, %3.1f", temperature);
	send_msg(sbuff);
	
	sprintf(sbuff, "humidity, %02d", humidity);
	send_msg(sbuff);
	
	sprintf(sbuff, "pressure, %04d", pressure);
	send_msg(sbuff);
	
	sprintf(sbuff, "pm1, %03d", pm1);
	send_msg(sbuff);
	
	sprintf(sbuff, "pm2, %03d", pm2);
	send_msg(sbuff);
	
	sprintf(sbuff, "pm10, %03d", pm10);
	send_msg(sbuff);
	
	sprintf(sbuff, "carbonmonoxide, %04d", co);
	send_msg(sbuff);
	
	sprintf(sbuff, "co2_ppm, %04d", co2);
	send_msg(sbuff);
	
	sprintf(sbuff, "o3, %04d", o3);
	send_msg(sbuff);
	
	if (receive_is("OK"))		//receive <OK> from o3?
		return true;
	else
		return false;
}


bool ESP8266::receive_is(const char * msg)
{
	char rx[MAX_MSG_LEN];
	int i = 0;
	
	if (receive_encapsulated(rx, &i))
	{
		
	}
	
	if (strncmp(rx, msg, MAX_MSG_LEN))		//if strings match
		return true;
	else
		return false;
}

bool ESP8266::receive_encapsulated(char * msg, int * num)
{
//----------------------------------------
// Serial receive buffer
	const int numChars = 64;
	bool newData = false;
	bool recvInProgress = false;
	int ndx = 0;
	uint8_t startMarker = '<';
	uint8_t endMarker = '>';
	uint8_t rc = 0;
	
	memset(RxBuffer, 0, ARR_LEN(RxBuffer));		//Clear Rx buffer to avoid confusion
	//if(sizeof)

	while (newData == false) 
	{
		if (HAL_UART_Receive(uart, &rc, 1, 100) != HAL_OK)		//receive ONE character
			return false;

		if (recvInProgress == true) 
		{
			if (rc != endMarker) 
			{
				msg[ndx] = rc;
				ndx++;
				if (ndx >= numChars) {
					ndx = numChars - 1;
				}
			}
		
			else 
			{
				msg[ndx] = '\0'; // terminate the string
				recvInProgress = false;
				ndx = 0;
				newData = true;
				//return true
			}
		}
		
		else if(rc == startMarker) 
		{
			recvInProgress = true;
		}
	}

	return true;
}


//-----------------------------------------------------------------------------
bool ESP8266::send_msg(const char * msg)
{
	uint8_t sbuff[MAX_MSG_LEN] = { '<' };
	int i, msgLen;

	if (sizeof(msg) > 100)	//test
		return false;
	
	for (i = 0; i < 32; i++)
	{
		if (msg[i] == 0)
			break;
		sbuff[i + 1] = msg[i];
	}
	
	sbuff[i + 1] =	'>';
	sbuff[i + 2] =	'\n';		//CR LF?
	
	msgLen = i + 2;
	
	HAL_UART_AbortReceive(uart);			//clear in case
	
	if (HAL_UART_Transmit(uart, sbuff, msgLen, 50) != HAL_OK)
		return false;
	return true;
}

//-----------------------------------------------------------------------------
bool ESP8266::receive_msg(ESP_Msg msg)
{
	if (sizeof(msg) > MAX_MSG_LEN)	//test
		return false;

	if (HAL_UART_Receive(uart, (uint8_t *)RxBuffer, msg_len(msg), 100) != HAL_OK)
		return false;
	
	return true;
}

uint8_t ESP8266::msg_len(ESP_Msg msg)
{
	switch (msg)
	{
	case Status: return 6 + 1;
	case Time: return 4 + 1;
	case LED: return 3;
	case NTP: return 27;
	case Shutdown: return 8;
	default: return 0;
	}
}

uint32_t ESP8266::str_len(uint8_t * string)
{
	return strlen(reinterpret_cast<char *>(string));
}

//-----------------------------------------------------------------------------
void wifi_post_data(void)
{
	
}

