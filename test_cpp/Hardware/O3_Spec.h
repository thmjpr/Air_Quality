/**
  ******************************************************************************
  * @file    O3_Spec.h
  * @author  
  * @version 
  * @date    Aug 2017
  * @brief   
  ******************************************************************************
  * 
  */

/* Specifications:
- Measurement Range 0 to 5 ppm
- Lower Detection Limit <  20 ppb 
- Resolution < 20 ppb 
- Response Time < 15 seconds typical
- Sensitivity nA/ppm: 12nA/ppm +/- 2
- Expected Operating Life > 5 years (10 years @ 23C 40+/- 10% RH)
- Operating Temperature Range - 3 0 to 50 C (-20 to 40C continuous)
- Operating Humidity Range � non-condensing 0 to 100% RH (15 to 95% continuous)
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
 

class SPEC_O3
{
public:
	SPEC_O3();
	~SPEC_O3();
	
	/**
	* @brief Initialize sensor
	* @param  
	* @retval  
	*/
	bool init(void);
	
	/**
	  * @brief  Power on the i2c port of AFE sensor
	  * @param  
	  * @retval  
	  * @note	Its not really necessary to power down AFE, as consumption is so low (10uA run)
	  */
	void power(bool power);

	/**
	* @brief 
	* @param
	* @retval
	*/
	void o3_request(void);
	
	/**
	  * @brief  Calculate offsets/cal/etc and return actual reading in PPM
	  * @param  
	  * @retval  
	  */	
	int get_ppm(float ambient_temp, uint32_t adc);

	/**
	  * @brief  Calculate offsets/cal/etc and return actual reading in PPB
	  * @param  
	  * @retval  
	  */	
	int get_ppb(float ambient_temp, uint32_t adc);
	
	/**
	* @brief
	* @param
	* @retval
	*/
	int read_adc(void);
	
	/**
	* @brief
	* @param
	* @retval
	*/
	int read_blocking(float temperature);
	
	bool sensor_present();
	int get_average();
	bool present;
	
private:
	
};


/*-------------------------------END OF FILE-------------------------------*/