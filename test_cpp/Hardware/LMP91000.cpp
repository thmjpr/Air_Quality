/**
  ******************************************************************************
  * @file    LMP91000.c
  * @author  Thomas Price + TI
  * @version 
  * @date    Oct 1, 2016
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "i2c.h"
#include "LMP91000.h"
#include "mxconstants.h"
#include "Hardware/CO_110_Spec.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TEMP_EQN_OFFSET 1562.2                                                 // LMP91000 DS pg 12: V=(-8.16mV/°C)*T+1562.2mV
#define TEMP_EQN_SLOPE  8.16                                                   // Linear approximation over temp range 20C to 50C
#define VREF            2.50
#define VREFmv          (VREF*1000)
#define ADC12_RATIO     (VREFmv/4096)                                         // 2500/4096 (2.5V reference & 12bit converter)


/************************************************************
* TI LMP91000 REGISTER SET ADDRESSES
************************************************************/

#define I2C_ADDR			                        (0x48)                  // Device Address

#define LMP91000_STATUS_REG                         (0x00)                  /* Read only status register */
#define LMP91000_LOCK_REG                           (0x01)                  /* Protection Register */
#define LMP91000_TIACN_REG                          (0x10)                  /* TIA Control Register */
#define LMP91000_REFCN_REG                          (0x11)                  /* Reference Control Register*/
#define LMP91000_MODECN_REG                         (0x12)                  /* Mode Control Register */

#define LMP91000_WRITE_LOCK                         (0x01)
#define LMP91000_WRITE_UNLOCK                       (0x00)
#define LMP91000_READY                              (0x01)
#define LMP91000_NOT_READY                          (0x00)

/************************************************************
* TI LMP91000 REGISTER SET INITIALIZATION VALUES
************************************************************/

constexpr uint8_t LMP91000_TIACN_REG_VALUE =		(0x03);
constexpr uint8_t LMP91000_REFCN_REG_VALUE =		(0x20);       
constexpr uint8_t LMP91000_MODECN_REG_VALUE =		(0x00);


//******************************************************************************
Lmp91000::Lmp91000()
{
}
 
//******************************************************************************
Lmp91000::~Lmp91000()
{
}
 
//******************************************************************************
int Lmp91000::reset(void)
{
	/*
	if (i2c_transmit(I2C_ADDR, cmd_rst, sizeof(cmd_rst)) != 0)
		return -1;
	*/
	return 0;
}
 

//-----------------------------------------------------------------------------
int Lmp91000::init()
{
	int read_val[4];

	read_val[0] = lmp_read_reg(LMP91000_LOCK_REG);
	read_val[1] = lmp_read_reg(LMP91000_TIACN_REG);
	read_val[2] = lmp_read_reg(LMP91000_REFCN_REG);
	read_val[3] = lmp_read_reg(LMP91000_MODECN_REG);
	
	lmp_write_reg(LMP91000_LOCK_REG, LMP91000_WRITE_UNLOCK);					//Unlock to allow writes
	lmp_write_reg(LMP91000_TIACN_REG, TIA_GAIN_120K);							//Set amp gain
	read_val[1] = lmp_read_reg(LMP91000_TIACN_REG);
	lmp_write_reg(LMP91000_MODECN_REG, FET_SHORT_N | OP_MODE_3LEAD);			//remove short and set mode to 3-lead amperometric cell
	
	if (read_val[1] == TIA_GAIN_120K)											//Check that the gain we wrote to LMP reads back as the same value
	{
		return 0;	
	}
	
	else
		return -1;

	return 0;
}

//******************************************************************************
bool Lmp91000::isReady()
{
	uint8_t status;
	status = lmp_read_reg(LMP91000_STATUS_REG) & 0x01;
	
	if (status)
		return true;		//Device ready for i2c commands
	else 
		return false;
}


//******************************************************************************
int Lmp91000::configGain(Lmp91000::TIACN gain)
{
	//uint8_t data[2];	
	return	lmp_write_reg(LMP91000_TIACN_REG, gain);
}


//******************************************************************************
int Lmp91000::configReference(Lmp91000::REFCN source, REFCN zero = INT_ZERO_50)
{
	//uint8_t data[2];	
	return lmp_write_reg(LMP91000_REFCN_REG, source | zero);
}

//******************************************************************************
int Lmp91000::configMode(Lmp91000::MODECN mode, Lmp91000::MODECN mode2)
{
	//uint8_t data[2];	
	return lmp_write_reg(LMP91000_MODECN_REG, mode | mode2);
}

//******************************************************************************
int Lmp91000::lock()
{
	return lmp_write_reg(LMP91000_LOCK_REG, LMP91000_WRITE_LOCK);
}

//******************************************************************************
int Lmp91000::unlock()
{
	return lmp_write_reg(LMP91000_LOCK_REG, LMP91000_WRITE_UNLOCK);
}

//******************************************************************************
int Lmp91000::enableDiagnostic(bool enable)
{		//should read then write
	if (enable)
		return lmp_write_reg(LMP91000_REFCN_REG, REFCN_INT | INT_ZERO_50 | DIAG_1);
	else
		return lmp_write_reg(LMP91000_REFCN_REG, REFCN_INT | INT_ZERO_50 | DIAG_0);
	
}

//******************************************************************************

uint8_t Lmp91000::lmp_read_reg(uint8_t reg)
{
	i2c_transmit_single(I2C_ADDR, reg);
	return i2c_receive_single(I2C_ADDR);
	return 0;
}

int Lmp91000::lmp_write_reg(uint8_t reg, uint8_t data)
{
	uint8_t buf[2] = { reg, data };
	return i2c_transmit(I2C_ADDR, buf, 2);
}