/**
  ******************************************************************************
  * @file    O3_Spec.c
  * @author  Thomas Price
  * @version 
  * @date    Aug, 2017
  * @brief   Ozone sensor, connected to AFE LMP91000
  ******************************************************************************
  * @attention
  * There is about 10min powerup time before the reading is stable
  * http://www.spec-sensors.com/wp-content/uploads/2016/05/SPEC-AN-103-Power-On-Stabilization.pdf
  * There are cross-sensitivities of concern: NO2, H2S, chlorine.
  *
  * Could combine o3 + CO into same file since its so similar.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "O3_Spec.h"
#include "adc.h"
#include "mxconstants.h"
#include "main.h"
#include "LMP91000.h"
#include "stdbool.h"
#include "stdlib.h"
#include "math.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define LMP_TIA_GAIN	350000		//Gain set by register or external resistor
#define O3_V_OFFSET		0.0282		//Offset adjustment
#define AMP_PER_NA		0.000000001	//??
#define V_PER_KV		1000
#define O3_RANGE		20			//ppm range of sensor	
#define VOLTAGE_REF		2.048		//Reference voltage for Vdda and LMP Vref

float na_per_ppm = -11.68;			//na per ppm is calibration value written on sticker of sensor
float ppm_scale;
int adc_buff[32] = { 0 };
Lmp91000 afe;


//******************************************************************************
SPEC_O3::SPEC_O3()
{
	present = false;
}

//******************************************************************************
SPEC_O3::~SPEC_O3()
{
}

//******************************************************************************
void SPEC_O3::power(bool power)
{
	if (power)
	{//Active low CO_ENABLE
		HAL_GPIO_WritePin(CO_En_GPIO_Port, CO_En_Pin, GPIO_PIN_RESET);
	}
	else
	{
		HAL_GPIO_WritePin(CO_En_GPIO_Port, CO_En_Pin, GPIO_PIN_SET);
	}
}

//******************************************************************************
bool SPEC_O3::init(void)
{
	power(true);
	delay_ms(1);
	
	if (!afe.isReady())
		return false;
	
	afe.unlock();
	afe.configReference(Lmp91000::REFCN_EXT, Lmp91000::INT_ZERO_67);		//67% reference
	afe.configGain(Lmp91000::TIA_GAIN_350K);								//350K gain is highest
	afe.configMode(Lmp91000::OP_MODE_3LEAD, Lmp91000::FET_SHORT_N);			//3-lead mode for CO
	
	//TIA gain for O3 specified as 499k, 350k is max of LMP, even when using an external resistor.
	
	//ISENSOR * Gain = Rtia x10^-4 A <= 2.048V
	//Sensor = 0 to -60nA. So 0 to 0.021V. Would likely need to use lower voltage reference. Mininum Vref for LMP is 1.5V, minimum for STM32 is 1.62V (Vdda) **FFF 
	
	
	//can some cal be performed with fet shorted? to determine 67% voltage.
	
	//http://www.spec-sensors.com/wp-content/uploads/2016/06/User-Manual-SPEC-Sensors-SDK-17.pdf
	//ppm per volt on ADC
	ppm_scale = na_per_ppm * LMP_TIA_GAIN * AMP_PER_NA * V_PER_KV;			//Sensitivity code * TIA gain * nA * V
	
	present = true;
	
	return 0;
}

//******************************************************************************
int SPEC_O3::read_adc(void)
{
	uint32_t i;

	ADC_ChannelConfTypeDef sConfig;

	sConfig.Channel = ADC_CHANNEL_15;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_92CYCLES_5;			//
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
		Error_Handler();	
	
	HAL_ADC_Start(&hadc1);							//Start ADC conversion
	HAL_ADC_PollForConversion(&hadc1, 10);			//Wait until finished, 10ms timeout
	
	//Must perform two reads to avoid errata 2.4.4 (if reads are longer than 1ms apart).
	
	for (i = 0; i < ARR_LEN(adc_buff); i++)
	{
		HAL_ADC_Start(&hadc1);
		HAL_ADC_PollForConversion(&hadc1, 10);			//10ms timeout. Actual conversion time = 
		adc_buff[i] = HAL_ADC_GetValue(&hadc1);
	}
	
	return get_average();
}

//******************************************************************************
int SPEC_O3::read_blocking(float temperature)
{
	int adc, res;
	
	adc = read_adc();
	res = get_ppb(temperature, adc);
	
	return res;
}


//******************************************************************************
//Check what 0 result gives
//check for overflows..
int SPEC_O3::get_average(void)
{
	uint32_t avg = 0;
	
	for (uint32_t i = 0; i < ARR_LEN(adc_buff); i++)
	{
		avg += adc_buff[i];	
	}
	
	return (avg + ARR_LEN(adc_buff) / 2) / ARR_LEN(adc_buff);     //round up and return average
}


//******************************************************************************
//
int SPEC_O3::get_ppb(float ambient_temp, uint32_t adc)
{
	float Vgas;
	float Vref = VOLTAGE_REF * 0.67;				//Reference set as 67%
	float ppb_reading;
	
	Vgas = adc / (0xFFF / VOLTAGE_REF);
	ppb_reading = 1000 * ppm_scale * (Vgas - Vref - O3_V_OFFSET);
	
	//Temperature coefficient compensation (from datasheet "Temperature Effect")
	//Increased output at higher temperatures
	ppb_reading *= (100 / (ambient_temp * 0.2984 + 91.786));
	
	//Zero shift compensation
	if (ambient_temp > 20)
	{
		ppb_reading -= 5.2 * (ambient_temp - 20);		//Not so accurate	
	}

	ppb_reading += 0.5;									//round up
	
	if (ppb_reading > (O3_RANGE * 2 * 1000))
		ppb_reading = O3_RANGE * 2 * 1000;
	//else if (ppb_reading < 0)
	//	ppb_reading = 0;
	
	return ppb_reading;//(uint32_t)ppb_reading;
}

//******************************************************************************
bool SPEC_O3::sensor_present(void)
{
	//put REFCN to 1% then delay, then back to 0%
	afe.enableDiagnostic(true);

	HAL_Delay(1000);

	//The output should charge up and decay
	//can test health of sensor and if sensor is present

	afe.enableDiagnostic(false);
	return true;
}