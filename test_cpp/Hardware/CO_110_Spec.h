/**
  ******************************************************************************
  * @file    CO_110_Spec.h
  * @author  
  * @version 
  * @date    2016
  * @brief   
  ******************************************************************************
  * 
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
 

class SPEC_CO_110
{
public:
	SPEC_CO_110();
	~SPEC_CO_110();
	
	/**
	* @brief Initialize sensor
	* @param  
	* @retval  
	*/
	bool init(void);
	
	/**
	  * @brief  Power on the i2c port of AFE sensor
	  * @param  
	  * @retval  
	  * @note	Its not really necessary to power down AFE, as consumption is so low (10uA run)
	  */
	void power(bool power);

	/**
	* @brief 
	* @param
	* @retval
	*/
	void co_request(void);
	
	/**
	  * @brief  Calculate offsets/cal/etc and return actual reading in PPM
	  * @param  
	  * @retval  
	  */	
	int get_ppm(float ambient_temp, uint32_t adc);

	/**
	  * @brief  Calculate offsets/cal/etc and return actual reading in PPB
	  * @param  
	  * @retval  
	  */	
	int get_ppb(float ambient_temp, uint32_t adc);
	
	/**
	* @brief
	* @param
	* @retval
	*/
	int read_adc(void);
	
	/**
	* @brief
	* @param
	* @retval
	*/
	int read_blocking(float temperature);
	
	bool sensor_present();
	int get_average();
	bool present;
	
private:
	
};


/*-------------------------------END OF FILE-------------------------------*/




