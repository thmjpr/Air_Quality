/**
  ******************************************************************************
  * @file    i2c.c
  * @author  Thomas Price
  * @version 
  * @date    Oct 1, 2016
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "mxconstants.h"
#include "error.h"
#include "main.h"
#include "i2c.h"

#define I2C_TX_BUFFER 32
#define I2C_RX_BUFFER 32

/* Globals ------------------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

/* Functions ------------------------------------------------------------------*/

/* I2C1 init function */
void MX_I2C1_Init(void)
{
	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing = 0x10909CEC;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c1) != HAL_OK)
		Error_Handler();

		/**Configure Analogue filter 
		*/
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
		Error_Handler();
}


bool i2c_transmit_single(uint8_t address, uint8_t data)
{
	uint8_t buf[1] = { data };
	uint32_t timeout = 0;
 /*##-2- Start the transmission process #####################################*/  
  /* While the I2C in reception process, user can transmit data through 
	 "aTxBuffer" buffer */
	do
	{
		if (HAL_I2C_Master_Transmit_DMA(&hi2c1, (uint16_t)address << 1, buf, 1) != HAL_OK)
		{
		  /* Error_Handler() function is called when error occurs. */
			Error_Handler();
		}

			/*##-3- Wait for the end of the transfer #################################*/  
			/*  Before starting a new communication transfer, you need to check the current   
				state of the peripheral; if it�s busy you need to wait for the end of current
				transfer before starting a new one.
				For simplicity reasons, this example is just waiting till the end of the 
				transfer, but application may perform other tasks while transfer operation
				is ongoing. */  
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
		{
		} 

			/* When Acknowledge failure occurs (Slave don't acknowledge it's address)
			   Master restarts communication */
	} while ((HAL_I2C_GetError(&hi2c1) == HAL_I2C_ERROR_AF) & (timeout++ < 0xF));
 
	return true;
}

uint8_t i2c_transmit(uint8_t address, uint8_t * data, uint8_t data_len)
{
	uint32_t timeout = 0;

 /*##-2- Start the transmission process #####################################*/  
  /* While the I2C in reception process, user can transmit data through 
	 "aTxBuffer" buffer */
	do
	{
		if (HAL_I2C_Master_Transmit_DMA(&hi2c1, (uint16_t)address << 1, data, data_len) != HAL_OK)
		{
		  /* Error_Handler() function is called when error occurs. */
			Error_Handler();
		}

			/*##-3- Wait for the end of the transfer #################################*/  
			/*  Before starting a new communication transfer, you need to check the current   
				state of the peripheral; if it�s busy you need to wait for the end of current
				transfer before starting a new one.
				For simplicity reasons, this example is just waiting till the end of the 
				transfer, but application may perform other tasks while transfer operation
				is ongoing. */  
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
		{
		} 

			/* When Acknowledge failure occurs (Slave don't acknowledge it's address)
			   Master restarts communication */
	} while ((HAL_I2C_GetError(&hi2c1) == HAL_I2C_ERROR_AF) & (timeout++ <0xF));
 
	
	//could implement timeout and return failure
	return 0;
}


uint8_t i2c_receive_single(uint8_t address)
{
	uint8_t rx_buffer[2] = { 0 };
	uint32_t timeout = 0;

  /*##-4- Put I2C peripheral in reception process ###########################*/  
	do
	{
		if (HAL_I2C_Master_Receive_DMA(&hi2c1, (uint16_t)address << 1, (uint8_t *)rx_buffer, 1) != HAL_OK)
		{
		  /* Error_Handler() function is called when error occurs. */
			Error_Handler();
		}

			/*##-5- Wait for the end of the transfer #################################*/  
			/*  Before starting a new communication transfer, you need to check the current   
				state of the peripheral; if it�s busy you need to wait for the end of current
				transfer before starting a new one.
				For simplicity reasons, this example is just waiting till the end of the 
				transfer, but application may perform other tasks while transfer operation
				is ongoing. */  
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
		{} 

			/* When Acknowledge failure occurs (Slave don't acknowledge it's address)
			   Master restarts communication */
	} while ((HAL_I2C_GetError(&hi2c1) == HAL_I2C_ERROR_AF) & (timeout++ < 0xF));

	
  /*##-5- Wait for the end of the transfer ###################################*/  
  /*  Before starting a new communication transfer, you need to check the current   
	  state of the peripheral; if it�s busy you need to wait for the end of current
	  transfer before starting a new one.
	  For simplicity reasons, this example is just waiting till the end of the
	  transfer, but application may perform other tasks while transfer operation
	  is ongoing. */
	timeout = 0;
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	{
		if (timeout++ > 0xFF)
			return 0;
	} 
  
	return rx_buffer[0];
}

uint8_t i2c_receive(uint8_t address, uint8_t * rx_data, uint8_t data_len)
{
	uint32_t timeout = 0;
  /*##-4- Put I2C peripheral in reception process ###########################*/  
	do
	{
		if (HAL_I2C_Master_Receive_DMA(&hi2c1, (uint16_t)address << 1, (uint8_t *)rx_data, data_len) != HAL_OK)
		{
		  /* Error_Handler() function is called when error occurs. */
			Error_Handler();
		}

			/*##-5- Wait for the end of the transfer #################################*/  
			/*  Before starting a new communication transfer, you need to check the current   
				state of the peripheral; if it�s busy you need to wait for the end of current
				transfer before starting a new one.
				For simplicity reasons, this example is just waiting till the end of the 
				transfer, but application may perform other tasks while transfer operation
				is ongoing. */  
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
		{
		} 

			/* When Acknowledge failure occurs (Slave don't acknowledge it's address)
			   Master restarts communication */
	} while ((HAL_I2C_GetError(&hi2c1) == HAL_I2C_ERROR_AF) & (timeout++ < 0x0F));

	
  /*##-5- Wait for the end of the transfer ###################################*/  
  /*  Before starting a new communication transfer, you need to check the current   
	  state of the peripheral; if it�s busy you need to wait for the end of current
	  transfer before starting a new one.
	  For simplicity reasons, this example is just waiting till the end of the
	  transfer, but application may perform other tasks while transfer operation
	  is ongoing. */
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	{
	} 
  
	return 0;
	//**FFF shoudl return state return hi2c1.ErrorCode;
}