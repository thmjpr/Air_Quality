/**
  ******************************************************************************
  * @file    SHT15_temp.h
  * @author  Thomas
  * @version 
  * @date    
  * @brief  
  ******************************************************************************


  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

#ifdef __cplusplus
extern "C" {
#endif


//Sensor is constantly sending!!

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "stdbool.h"
#include "stdint.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

	 
#ifdef __cplusplus
}
#endif


/*-------------------------------END OF FILE-------------------------------*/




