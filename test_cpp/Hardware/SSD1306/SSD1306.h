/**
  ******************************************************************************
  * @file    SSD1306.h
  * @author  Waveshare Team
  * @version 
  * @date    13-October-2014
  * @brief   This file contains all the functions prototypes for the SSD1306 OLED firmware driver.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, WAVESHARE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _SSD1306_H_
#define _SSD1306_H_
	
#ifdef __cplusplus
	extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "MacroAndConst.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define DISP_W (128-1)
#define DISP_H (64-1)

#define SSD1306_SETCONTRAST			0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON		0xA5
#define SSD1306_NORMALDISPLAY		0xA6
#define SSD1306_INVERTDISPLAY		0xA7
#define SSD1306_DISPLAYOFF			0xAE
#define SSD1306_DISPLAYON			0xAF

#define SSD1306_SETDISPLAYOFFSET	0xD3
#define SSD1306_SETCOMPINS			0xDA

#define SSD1306_SETVCOMDETECT		0xDB

#define SSD1306_SETDISPLAYCLOCKDIV	0xD5
#define SSD1306_SETPRECHARGE		0xD9

#define SSD1306_SETMULTIPLEX		0xA8

#define SSD1306_SETLOWCOLUMN		0x00
#define SSD1306_SETHIGHCOLUMN		0x10

#define SSD1306_SETSTARTLINE		0x40

#define SSD1306_MEMORYMODE			0x20
#define SSD1306_COLUMNADDRESS		0x21
#define SSD1306_PAGEADDRESS			0x22

#define SSD1306_COMSCANINC			0xC0
#define SSD1306_COMSCANDEC			0xC8

#define SSD1306_SEGREMAP			0xA0

#define SSD1306_CHARGEPUMP			0x8D
#define SSD1306_EN_CHARGEPUMP		0x14
#define SSD1306_DIS_CHARGEPUMP		0x10


#define SSD1306_EXTERNALVCC			0x1
#define SSD1306_SWITCHCAPVCC		0x2

#define ssd1306_write_cmd(arg)	ssd1306_write_byte(arg, SSD1306_CMD)

	typedef enum
	{
		FREQ_5,
		FREQ_64,
		FREQ_128,
		FREQ_256,
		FREQ_3,
		FREQ_4,
		FREQ_25,
		FREQ_2
	}SSD1306_FREQ;

	/* Exported macro ------------------------------------------------------------*/

	/* Exported functions ------------------------------------------------------- */

	extern void ssd1306_clear_screen(uint8_t chFill);
	extern void ssd1306_update(void);
	extern void ssd1306_flip_screen(bool flipped);
	extern void ssd1306_rotate_screen(bool rot);
	extern void ssd1306_invert_screen(bool invert);
	extern void ssd1306_start_horizontal_scroll(uint8_t direction, uint8_t start, uint8_t end, SSD1306_FREQ interval);
	extern void ssd1306_start_vertical_and_horizontal_scroll(uint8_t direction, uint8_t start, uint8_t end, SSD1306_FREQ interval, uint8_t vertical_offset);
	extern void ssd1306_stop_scroll();

	extern void ssd1306_set_contrast(uint8_t contrast);

	extern void ssd1306_draw_point(uint8_t chXpos, uint8_t chYpos, uint8_t chPoint);
	extern void ssd1306_fill_screen(uint8_t chXpos1, uint8_t chYpos1, uint8_t chXpos2, uint8_t chYpos2, uint8_t chDot);
	extern void ssd1306_display_char(uint8_t chXpos, uint8_t chYpos, uint8_t chChr, uint8_t chSize, uint8_t chMode);
	extern void ssd1306_display_num(uint8_t chXpos, uint8_t chYpos, uint32_t chNum, uint8_t chLen, uint8_t chSize);
	extern void ssd1306_display_string(uint8_t x, uint8_t y, const uint8_t *pchString, uint8_t chSize, uint8_t chMode);
	extern void ssd1306_display_string_up(uint8_t x, uint8_t y, const uint8_t *pchString, uint8_t chSize, uint8_t chMode);

	extern void ssd1306_draw_3216str(uint8_t chXpos, uint8_t chYpos, uint8_t* str);
	extern void ssd1306_draw_str(uint8_t chXpos, uint8_t chYpos, uint8_t* str);



	extern void ssd1306_draw_1616char(uint8_t chXpos, uint8_t chYpos, uint8_t chChar);
	extern void ssd1306_draw_3216char(uint8_t chXpos, uint8_t chYpos, uint8_t chChar);
	extern void ssd1306_draw_bitmap(uint8_t chXpos, uint8_t chYpos, const uint8_t *pchBmp, uint8_t chWidth, uint8_t chHeight);
	extern void ssd1306_draw_bitmap_a(uint8_t chXpos, uint8_t chYpos, const uint8_t *pchBmp, uint8_t chWidth, uint8_t chHeight);
		
	extern void ssd1306_draw_line(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t chPoint);

	extern void ssd1306_init(void);
	
		#ifdef __cplusplus
}
#endif
	  
#endif

/*-------------------------------END OF FILE-------------------------------*/


