

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once
#ifdef __cplusplus
extern "C" {
#endif
	 
/* Includes ------------------------------------------------------------------*/
#include "LIB_Config.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

	typedef enum
	{
		FONT_V_SMALL = 10,
		//FONT_SMALL = 9,
		FONT_MED     = 12,
		FONT_LARGE   = 16,
	}FONT_SIZE;

	/* Exported macro ------------------------------------------------------------*/
	/* Exported functions ------------------------------------------------------- */

	extern const uint8_t c_chFont0806[95][8];
	extern const uint8_t c_chFont1206[95][12];
	extern const uint8_t c_chFont1608[95][16];
	extern const uint8_t c_chFont1612[11][32];
	extern const uint8_t c_chFont3216[12][64];

	extern const uint8_t c_chBmp4016[96];
	extern const uint8_t c_chSingal816[16];
	extern const uint8_t c_chMsg816[16];
	extern const uint8_t c_chBluetooth88[8];
	extern const uint8_t c_chBat816[16];
	extern const uint8_t c_chGPRS88[8];
	extern const uint8_t c_chAlarm88[8];

	//icons
	extern const uint8_t icon_ugm3_small[];
	extern const uint8_t icon_ugm3[];
	extern const uint8_t icon_ppm_small[];
	extern const uint8_t icon_ppm[];
	extern const uint8_t icon_degc_small[];
	extern const uint8_t icon_degc[];
	extern const uint8_t icon_pascal_small[];
	extern const uint8_t icon_CO2[];
	extern const uint8_t icon_wifi[11];
	extern const uint8_t icon_download[8];
	extern const uint8_t icon_up_arrow[6];
	extern const uint8_t icon_down_arrow[6];
	extern const uint8_t icon_splash_screen[];

	#ifdef __cplusplus
}
#endif
/*-------------------------------END OF FILE-------------------------------*/

