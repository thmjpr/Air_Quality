/**
  ******************************************************************************
  * @file    ESP8266_wifi.h
  * @author  Thomas
  * @version 
  * @date    
  * @brief   Wifi serial code

  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
//#include <map>
/*
static std::map< ESP_Msg, const char * > msg = {
	{ AA, "This is an apple" },
	{ BB, "This is a book" },
	{ CC, "This is a coffee" },
	{ DD, "This is a door" }
};*/

#include "stm32l4xx_hal.h"
#include "stdbool.h"
#include "stdint.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
	
	class ESP8266
	{
		enum ESP_Msg {
			Status,
			Time,
			LED,		//must have some way to include data?
			NTP,
			Shutdown,
		};

	public:
		ESP8266(UART_HandleTypeDef * uart_ptr);
		~ESP8266();
	
		/**
		* @brief Check that ESP8266 module connected and initialize
		* @param  
		* @retval  
		*/
		bool init(void);
	
		/**
		  * @brief  Power on the ESP8266
		  * @param  
		  * @retval  
		  * @note	
		  */
		bool power(bool power);
		bool present = false;
		bool get_status(void);
		bool get_time(char * time);
		bool get_mac(void);
		bool set_led(bool state);
		bool send_measurements(float temperature, int humidity, int pressure, int pm1, int pm2, int pm10, int co2, int co, int o3);
	
	private:

		UART_HandleTypeDef * uart;
		uint8_t RxBuffer[20];
		uint8_t TxBuffer[20];

		char mac[18] = "00:00:00:00:00:00";

		bool send_msg(const char * msg);
		bool receive_msg(ESP_Msg msg);
		
		bool receive_is(const char * msg);
		bool receive_encapsulated(char * msg, int * num);

		uint8_t msg_len(ESP_Msg msg);
		uint32_t str_len(uint8_t * string);
		
		const static int UART_BAUD = 115200;		//not sure how to use directly
		const static int MAX_MSG_LEN = 32;
	};
	

/*-------------------------------END OF FILE-------------------------------*/