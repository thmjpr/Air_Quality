/**
  ******************************************************************************
  * @file    CO2_MHZ.c
  * @author  Thomas Price
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *  About 18ms after CO2 is powered on, a 115kbaud bootloader message is sent, lasting until 17ms.
  *  Must disable serial during this time, or ignore the data.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "CO2_MHZ.h"
#include "mxconstants.h"
#include "delay.h"
#include "main.h"
#include "string.h"
#include "usart.h"

//******************************************************************************

static uint8_t START_MSG[] = { 'K', 'B', '2', '0', '0', '\n', 'r' };						//Startup packet of CO2 sensor
static uint8_t REQ_CO2_Packet[] = { 0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79 };		//Get CO2 reading packet


//******************************************************************************
CO2_MHZ::CO2_MHZ(UART_HandleTypeDef * uart_ptr)
{
	uart = uart_ptr;
}
 

//******************************************************************************
CO2_MHZ::~CO2_MHZ()
{
	uart = 0;
	present = false;
}

//******************************************************************************
void CO2_MHZ::power(bool power)
{
	if (power)
	{
		HAL_GPIO_WritePin(Pwr_5V_En_GPIO_Port, Pwr_5V_En_Pin, GPIO_PIN_SET);
	}
		
	else
	{
		HAL_GPIO_WritePin(Pwr_5V_En_GPIO_Port, Pwr_5V_En_Pin, GPIO_PIN_RESET);
	}
}


//******************************************************************************
bool CO2_MHZ::init(void)
{
	present = false;
	
	power(true);
	HAL_Delay(20);					//Delay for CO2 sensor to bootup and transmit entry command
	uint8_t abuf[3] = { 'a' };		//abort bootloader
	HAL_UART_Transmit(uart, (uint8_t*)abuf, 1, 50);
	delay_ms(5);
	MX_USART3_Init(9600);		//main program is at 9600 baud
	
	//After 350ms, device will transmit "KB200\n\r" here
	if (HAL_UART_Receive(uart, (uint8_t *)RxBuffer, 7, 500) != HAL_OK)
		return false;
	
	if (memcmp(RxBuffer, START_MSG, sizeof(START_MSG)))									//Check expected device responded
	{
		uint32_t co2_lvl;
		
		if (read_blocking(&co2_lvl))
			present = true;
	}
	
	
	return present;
}


//-----------------------------------------------------------------------------
void CO2_MHZ::request(void)
{
	//Prevent request if device not initialized
	if (!present)
		return;

//Send UART command to read co2 level
	if (HAL_UART_Transmit_DMA(uart, (uint8_t*)REQ_CO2_Packet, sizeof(REQ_CO2_Packet)) != HAL_OK)
		Error_Handler();
//Setup UART DMA receive		
	if (HAL_UART_Receive_DMA(uart, (uint8_t *)RxBuffer, sizeof(RxBuffer)) != HAL_OK)
		Error_Handler();
}


//-----------------------------------------------------------------------------
uint32_t CO2_MHZ::get_co2_level()
{
	volatile int level;
	
	if (mh_checksum(RxBuffer) == RxBuffer[8])
	{
		level = RxBuffer[2] * 256 + RxBuffer[3];
		return level;
	}
	else
		return -1;				//error condition
}

//-----------------------------------------------------------------------------
bool CO2_MHZ::read_blocking(uint32_t * co2)
{
	HAL_UART_Transmit(uart, (uint8_t*)REQ_CO2_Packet, sizeof(REQ_CO2_Packet), 50);			//request for co2 reading	

	if (HAL_UART_Receive(uart, (uint8_t *)RxBuffer, sizeof(RxBuffer), 100) != HAL_OK)		//receive reading
		return false;

	*co2 = get_co2_level();
	
	return true;
}

//-----------------------------------------------------------------------------
uint8_t CO2_MHZ::mh_checksum(uint8_t* packet)
{
	uint8_t i, checksum = 0;
	
	for (i = 1; i < 8; i++)	
	{
		checksum += packet[i];
	}
	
	checksum = 0xFF - checksum;
	checksum += 1;
	return checksum;
}
