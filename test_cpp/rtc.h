/**
  ******************************************************************************
  * @file    rtc.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"

extern RTC_TimeTypeDef cTime;			//Current time
extern RTC_DateTypeDef cDate;			//Current date

extern RTC_TimeTypeDef nTime;
extern RTC_DateTypeDef nDate;	

extern RTC_HandleTypeDef hrtc;

/* Functions ------------------------------------------------------------------*/
extern void MX_RTC_Init(void);
extern void rtc_get_time_and_date(RTC_TimeTypeDef* time, RTC_DateTypeDef* date);
extern void rtc_set_time_and_date(RTC_TimeTypeDef* time, RTC_DateTypeDef* date);
extern "C" DWORD get_fattime();