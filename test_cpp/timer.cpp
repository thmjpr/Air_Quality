/**
  ******************************************************************************
  * @file    timer.cpp
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "timer.h"

TIM_HandleTypeDef htim1, htim2, htim16;

/* TIM1 init function */
void MX_TIM1_Init(void)
{
	/* Compute the prescaler value to have TIMx counter clock equal to 10000 Hz */
	uint32_t uwPrescalerValue = (uint32_t)(SystemCoreClock / 10000) - 1;

	htim1.Instance = TIM1;
	htim1.Init.Prescaler = uwPrescalerValue;				//10kHz
	htim1.Init.CounterMode = TIM_COUNTERMODE_UP;			//
	htim1.Init.Period = 5000 - 1;							//Set period to 0.5s
	htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim1.Init.RepetitionCounter = 0;
	
	//Send configuration to timer
	if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
		Error_Handler();
}

/* TIM2 init function */
void MX_TIM2_Init(void)
{
	/* Compute the prescaler value to have TIMx counter clock equal to 10000 Hz */
	uint32_t uwPrescalerValue = (uint32_t)(SystemCoreClock / 10000) - 1;

	htim2.Instance = TIM2;
	htim2.Init.Prescaler = uwPrescalerValue;			//10kHz
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;		//
	htim2.Init.Period = 10 - 1;							//Set period to 0.001s
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	
	//Send configuration to timer
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
		Error_Handler();
}

/* TIM16 init function */
//Timer 16 used for Piezo buzzer, resonant frequency of: 
void MX_TIM16_Init(void)
{
	TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;
	TIM_OC_InitTypeDef sConfigOC;

	// When counter reaches Period, will reset back to 0.
	// This is the ARR register. In this case, the clock is 80MHz so:
	//   80,000,000 / 12000 / 2 = Hz
	// The divide by two = takes two toggle to create one period	
	htim16.Instance = TIM16;
	htim16.Init.Prescaler = 0;
	htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim16.Init.Period = 13000;
	htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim16.Init.RepetitionCounter = 0;
	if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
		Error_Handler();

	if (HAL_TIM_OC_Init(&htim16) != HAL_OK)
		Error_Handler();

	sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
	sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
	sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
	sBreakDeadTimeConfig.DeadTime = 0;
	sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
	sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
	sBreakDeadTimeConfig.BreakFilter = 0;
	sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
	
	if (HAL_TIMEx_ConfigBreakDeadTime(&htim16, &sBreakDeadTimeConfig) != HAL_OK)
		Error_Handler();

	sConfigOC.OCMode = TIM_OCMODE_TOGGLE;				//Toggle pin when CNT >= CCR
	sConfigOC.Pulse = 0;								//This is the counter value when the the channel will be toggled
	sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
	sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
	sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	
	if (HAL_TIM_OC_ConfigChannel(&htim16, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
		Error_Handler();

	HAL_TIM_MspPostInit(&htim16);								//Setup output pin
	
	if (HAL_TIM_OC_Start(&htim16, TIM_CHANNEL_1) != HAL_OK)		//Start timer		//HAL_TIM_OC_Start_IT
		Error_Handler();
	HAL_Delay(3);												//Short beep on power up
	HAL_TIM_OC_Stop(&htim16, TIM_CHANNEL_1);
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	//static int count = 0;
	
	//Timer 1 0.5s
	if (htim->Instance == TIM1)
	{
	}
	
	//Timer 2 to manage piezo beep
	if (htim->Instance == TIM2)
	{
		if (piezo_on_time == 0)
		{
			HAL_TIM_Base_Stop(&htim2);
			HAL_TIM_OC_Stop(&htim16, TIM_CHANNEL_1);
		}
			
		
		if (--piezo_on_time < 0)
			piezo_on_time = -1;
	}
}

/**
  * @brief  Output Compare callback in non blocking mode 
  * @param  htim : TIM OC handle
  * @retval None
  */
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
}


/**
  * @brief  Activate the sensor reading timer
  * @param  Start or Stop the timer
  * @retval None
  */
void read_timer(bool start)
{	
	if (start)
	{
		//Start timer in interrupt mode
		if (HAL_TIM_Base_Start_IT(&htim1) != HAL_OK)
			Error_Handler();		
		read_timer_ = true;
	}
	else
	{
		if (HAL_TIM_Base_Stop_IT(&htim1) != HAL_OK)
			Error_Handler();
		read_timer_ = false;
	}
}