/**
  ******************************************************************************
  * @file    sd_card.h
  * @author  Thomas Price
  * @version 
  * @date    Oct 2016
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "ff.h"
#include "config_settings.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

//Structure of csv data:
// date/time 
// MM-DD-YY
// HH-MM-SS
// Temperature degrees C
// Humidity
// Pressure pascals
// Carbon monoxide content
// Carbon dioxide content
// PM1.0 = particles mean dia 1um
// PM2.5 = particles mean dia 2.5um 
// PM10 = particles mean dia 10um
// Reserved
// Status string (could log errors, etc.)
//SD_HandleTypeDef hsd1;


class SD_CARD
{
public:
	//Log data structure
	typedef struct
	{
		RTC_DateTypeDef date;	// char date[8];
		RTC_TimeTypeDef time;	// time[8];
		float temperature;
		uint8_t humidity;
		uint32_t pressure;
		uint32_t co2;
		uint32_t co;
		float pm1;
		float pm2;
		float pm10;
	}logData;
	
	//Add a define file for the SSID/PW and p
	typedef struct
	{
		bool key_beep;
		
		uint8_t wifi_ssid[15];
		uint8_t wifi_pw[15];
	}configData;
	
	SD_CARD();
	~SD_CARD();

	/**
	* @brief Initialize SD card driver
	* @param  
	* @retval  
	*/
	bool init(void);
	
	/**
	  * @brief  Ensure all log data is within a reasonable range, trim if not
	  * @param  
	  * @retval  
	  * @note	
	  */
	void sanitize_and_write(logData * log);
	
	/**
	  * @brief  Save a sensor reading to a new line in a CSV file
	  * @param  
	  * @retval  
	  * @note	
	  */
	void save_log(logData * log);
	
	/**
	  * @brief  Write the top line of CSV file with column titles
	  * @param  
	  * @retval  
	  * @note	
	  */
	void write_csv_header(FIL * log_file);
	
	/**
	  * @brief  Check that SD card is formatted and contains a log file that can be written
	  * @param  
	  * @retval  
	  * @note	
	  */	
	int32_t check_sd_formatted(RTC_DateTypeDef * nDate);
	
	
	/**
	  * @brief  Open config file from SD card and read settings
	  * @param  
	  * @retval  
	  * @note	
	  */	
	int32_t load_config_file(CONFIG * config);
	
	void write_new_line(FIL * log_file, char * str);
	void shutdown();

	bool present;			//Presence of SD card
	
private:	
	uint8_t retSD;			/* Return value for SD */
	char SD_Path[4];		/* SD logical drive path */
	FATFS SDFatFs;			//File system object for SD card drive
	FIL LogFile;			//File object for Log
	FIL ConfigFile;			//File object for config
	
	HAL_SD_CardInfoTypedef SDCardInfo1;		//Information about physical sd card (size, etc.)

};


/*-------------------------------END OF FILE-------------------------------*/
