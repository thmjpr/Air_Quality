/**
  ******************************************************************************
  * @file    usart.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"

extern UART_HandleTypeDef huart1, huart2, huart3;
extern __IO ITStatus Uart2_Rx_Ready;

/* Functions ------------------------------------------------------------------*/

extern void MX_USART1_Init(uint32_t baud);
extern void MX_USART2_Init(void);
extern void MX_USART3_Init(uint32_t baud);
