/**
  ******************************************************************************
  * @file    adc.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"

extern ADC_HandleTypeDef hadc1;

extern void MX_ADC1_Init(void);