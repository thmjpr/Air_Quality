/**
  ******************************************************************************
  * @file    dma.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"

/* DMA memory to memory transfer handles -------------------------------------*/
extern void Error_Handler(void);
extern void MX_DMA_Init(void);