/**
  ******************************************************************************
  * @file    adc.cpp
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

ADC_HandleTypeDef hadc1;

/* ADC1 init function */
void MX_ADC1_Init(void)
{
	ADC_MultiModeTypeDef multimode;
	ADC_ChannelConfTypeDef sConfig;

 /**Common config 
		*/
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV2;		//Max ADC clock is 80MHz
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc1.Init.LowPowerAutoWait = DISABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.NbrOfConversion = 1;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc1.Init.OversamplingMode = ENABLE;
	hadc1.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_4;
	hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_2;
	hadc1.Init.Oversampling.TriggeredMode = ADC_TRIGGEREDMODE_SINGLE_TRIGGER;
	hadc1.Init.Oversampling.OversamplingStopReset = ADC_REGOVERSAMPLING_CONTINUED_MODE;
	
	if (HAL_ADC_Init(&hadc1) != HAL_OK)
		Error_Handler();

		/**Configure the ADC multi-mode 
		*/
	multimode.Mode = ADC_MODE_INDEPENDENT;
	
	if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
		Error_Handler();

		/**Configure Regular Channel 
		*/
	sConfig.Channel = ADC_CHANNEL_15;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_92CYCLES_5;			//can tweak if needed, RAIN should be fine (pg152 datasheet)
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
		Error_Handler();
	
	//ADC must be disabled (execute this function before HAL_ADC_Start() or after HAL_ADC_Stop() ).
	//Important as it improves accuracy of ADC significantly
	HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
}


/**
  * @brief  Set ADC channel to battery voltage
  * @param  None
  * @retval None
  */
//Vbat should be 1.55V+
//Value is 1/3 of battery voltage
void adc_battery(void)
{
	ADC_ChannelConfTypeDef sConfig;

	sConfig.Channel = ADC_CHANNEL_VBAT;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;			//Requires 12us sampling time
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
		Error_Handler();
}

/**
  * @brief  Set ADC channel to VREF
  * @param  None
  * @retval None
  */
void adc_vref(void)
{
	ADC_ChannelConfTypeDef sConfig;

	sConfig.Channel = ADC_CHANNEL_VREFINT;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;			//Requires 4us sampling time
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
		Error_Handler();	
	
	//HAL_VREFINT_Cmd(DISABLE);
	//Vdda = 3V * VREFINT_CAL / VREFINT_DATA
}
