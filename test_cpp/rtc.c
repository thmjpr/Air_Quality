/**
  ******************************************************************************
  * @file    rtc.c
  * @author  Thomas
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "rtc.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef RtcHandle;

/* Buffers used for displaying Time and Date */
uint8_t aShowTime[50] = { 0 }, aShowTimeStamp[50] = { 0 };
uint8_t aShowDate[50] = { 0 }, aShowDateStamp[50] = { 0 };

/* Private function prototypes -----------------------------------------------*/
void RTC_mspinit(RTC_HandleTypeDef *hrtc);

/* Private functions ---------------------------------------------------------*/

void rtc_init()
{
	HAL_RTC_MspInit(&RtcHandle);
/*##-1- Configure the RTC peripheral #######################################*/
 /* Configure RTC prescaler and RTC data registers */
 /* RTC configured as follow:
     - Hour Format    = Format 12
     - Asynch Prediv  = Value according to source clock
     - Synch Prediv   = Value according to source clock
     - OutPut         = Output Disable
     - OutPutPolarity = High Polarity
     - OutPutType     = Open Drain */
	__HAL_RTC_RESET_HANDLE_STATE(&RtcHandle);
	RtcHandle.Instance = RTC;
	RtcHandle.Init.HourFormat     = RTC_HOURFORMAT_12;
	RtcHandle.Init.AsynchPrediv   = RTC_ASYNCH_PREDIV;
	RtcHandle.Init.SynchPrediv    = RTC_SYNCH_PREDIV;
	RtcHandle.Init.OutPut         = RTC_OUTPUT_DISABLE;
	RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	RtcHandle.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;
	
	if (HAL_RTC_Init(&RtcHandle) != HAL_OK)
	{
	  /* Initialization Error */
		Error_Handler(); 
	}

	  /*##-2-  Configure RTC Timestamp ############################################*/
	RTC_TimeStampConfig();
}


void RTC_MspInit(RTC_HandleTypeDef *hrtc)
{
	RCC_OscInitTypeDef        RCC_OscInitStruct;
	RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;
  
	/*##-1- Enables the PWR Clock and Enables access to the backup domain ###################################*/
	/* To change the source clock of the RTC feature (LSE, LSI), You have to:
	   - Enable the power clock using __HAL_RCC_PWR_CLK_ENABLE()
	   - Enable write access using HAL_PWR_EnableBkUpAccess() function before to 
	     configure the RTC clock source (to be done once after reset).
	- Reset the Back up Domain using __HAL_RCC_BACKUPRESET_FORCE() and 
	  __HAL_RCC_BACKUPRESET_RELEASE().
	- Configure the needed RTC clock source */
	__HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWR_EnableBkUpAccess();

	  /*##-2- Configue LSE/LSI as RTC clock soucre ###############################*/
#ifdef RTC_CLOCK_SOURCE_LSE
  
	RCC_OscInitStruct.OscillatorType =  RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_OFF;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{ 
		Error_Handler();
	}
  
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
	{ 
		Error_Handler();
	}
#elif defined (RTC_CLOCK_SOURCE_LSI)  
	RCC_OscInitStruct.OscillatorType =  RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{ 
		Error_Handler();
	}

	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
	{ 
		Error_Handler();
	}
#else
#error Please select the RTC Clock source inside the main.h file
#endif /*RTC_CLOCK_SOURCE_LSE*/
}
//RTC_CalendarShow()


/**
  * @brief  Configure the current time and date and activate timestamp.
  * @param  None
  * @retval None
  */
void RTC_TimeStampConfig(void)
{
	RTC_DateTypeDef sdatestructure;
	RTC_TimeTypeDef stimestructure;

	  /*##-1- Configure the Time Stamp peripheral ################################*/
	  /*  RTC TimeStamp generation: TimeStamp Rising Edge on PC.13 Pin */
	HAL_RTCEx_SetTimeStamp_IT(&RtcHandle, RTC_TIMESTAMPEDGE_RISING, RTC_TIMESTAMPPIN_DEFAULT);

	  /*##-2- Configure the Date #################################################*/
	  /* Set Date:  Jan 1 2016 */
	sdatestructure.Year    = 0x16;
	sdatestructure.Month   = RTC_MONTH_JANUARY;
	sdatestructure.Date    = 0x01;
	sdatestructure.WeekDay = RTC_WEEKDAY_FRIDAY;
  
	if (HAL_RTC_SetDate(&RtcHandle, &sdatestructure, RTC_FORMAT_BCD) != HAL_OK)
	{
	  /* Initialization Error */
		Error_Handler(); 
	} 
  
	/*##-3- Configure the Time #################################################*/
	/* Set Time: 08:00:00 */
	stimestructure.Hours          = 0x08;
	stimestructure.Minutes        = 0x00;
	stimestructure.Seconds        = 0x00;
	stimestructure.SubSeconds     = 0x00;
	stimestructure.TimeFormat     = RTC_HOURFORMAT12_AM;
	stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;
  
	if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, RTC_FORMAT_BCD) != HAL_OK)
	{
	  /* Initialization Error */
		Error_Handler(); 
	}
}

/**
  * @brief  Timestamp callback 
  * @param  hrtc : hrtc handle
  * @retval None
  */
void HAL_RTCEx_TimeStampEventCallback(RTC_HandleTypeDef *hrtc)
{
	RTC_DateTypeDef sTimeStampDateget;
	RTC_TimeTypeDef sTimeStampget;

	HAL_RTCEx_GetTimeStamp(&RtcHandle, &sTimeStampget, &sTimeStampDateget, RTC_FORMAT_BIN);

	  /* Display time Format : hh:mm:ss */
	sprintf((char*)aShowTimeStamp, "%.2d:%.2d:%.2d", sTimeStampget.Hours, sTimeStampget.Minutes, sTimeStampget.Seconds);
	/* Display date Format : mm-dd */
	sprintf((char*)aShowDateStamp, "%.2d-%.2d-%.2d", sTimeStampDateget.Month, sTimeStampDateget.Date, 2016);
}

/**
  * @brief  Display the current time and date.
  * @param  showtime : pointer to buffer
  * @param  showdate : pointer to buffer
  * @retval None
  */
void RTC_CalendarShow(void)
{
	RTC_DateTypeDef sdatestructureget;
	RTC_TimeTypeDef stimestructureget;
  
	/* Get the RTC current Time */
	HAL_RTC_GetTime(&RtcHandle, &stimestructureget, RTC_FORMAT_BIN);
	/* Get the RTC current Date */
	HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, RTC_FORMAT_BIN);
  
	/* Display time Format : hh:mm:ss */
	sprintf((char*)aShowTime, "%.2d:%.2d:%.2d", stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
	/* Display date Format : mm-dd-yy */
	sprintf((char*)aShowDate, "%.2d-%.2d-%.2d", sdatestructureget.Month, sdatestructureget.Date, 2000 + sdatestructureget.Year);
} 

//-----------------------------------------------------------------------------
/**
  * @brief  
  * @param  
  * @retval  
  */

void RTC_GetTime(RTC_TimeTypeDef *sdate)
{
	/* Get the RTC current Time */
	HAL_RTC_GetTime(&RtcHandle, sdate, RTC_FORMAT_BIN);	
}


//-----------------------------------------------------------------------------
/**
  * @brief  
  * @param  
  * @retval  
  */

void RTC_GetDate(RTC_DateTypeDef *stime)
{
	/* Get the RTC current Time */
	HAL_RTC_GetDate(&RtcHandle,stime, RTC_FORMAT_BIN);	
}

