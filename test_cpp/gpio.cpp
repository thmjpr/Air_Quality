/**
******************************************************************************
* File Name          : gpio.c
* Description        : This file provides code for the configuration
*                      of all used GPIO pins.
******************************************************************************
*
* COPYRIGHT(c) 2017 STMicroelectronics
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of STMicroelectronics nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"


/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/

/** Configure pins as
* Analog
* Input
* Output
* EVENT_OUT
* EXTI
*/
void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();

	/*Configure GPIO pins : PWM_Heater_Pin LD2_Pin WiFi_Shutdown_Pin */
	GPIO_InitStruct.Pin = PWM_Heater_Pin | Green_LED_Pin | WiFi_Shutdown_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIOB pins : Button_left_Pin Button_right_Pin Button_OK_Pin */
	//Set interrupt on button press falling
	GPIO_InitStruct.Pin = Button_left_Pin | Button_right_Pin | Button_OK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;		//Can do falling + rising too
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : Pwr_3V_en_Pin SPI_MI_Spare_Pin CO_En_Pin */
	GPIO_InitStruct.Pin = Pwr_3V_en_Pin | SPI_MI_Spare_Pin | CO_En_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : LCD_D_C_Pin LCD_CS_Pin LCD_Reset_Pin Pwr_5V_En_Pin */
	GPIO_InitStruct.Pin = LCD_D_C_Pin | LCD_CS_Pin | LCD_Reset_Pin | Pwr_5V_En_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : SD1_CS_Pin */
	GPIO_InitStruct.Pin = SD1_CS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(SD1_CS_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIOA pin Output Level */
	HAL_GPIO_WritePin(GPIOA, PWM_Heater_Pin | Green_LED_Pin | WiFi_Shutdown_Pin, GPIO_PIN_RESET);

	/*Configure GPIOB pin Output Level */
	HAL_GPIO_WritePin(GPIOB, Pwr_3V_en_Pin | Pwr_5V_En_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CO_En_Pin, GPIO_PIN_SET);

	/*Configure GPIOC pin Output Level */
	HAL_GPIO_WritePin(GPIOC, LCD_D_C_Pin | LCD_CS_Pin | LCD_Reset_Pin | Pwr_5V_En_Pin, GPIO_PIN_RESET);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 15, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 15, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}
