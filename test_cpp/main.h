/**
  ******************************************************************************
  * @file    main.h
  * @author  Thomas Price
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "stm32l4xx_hal.h"
#include "LIB_Config.h"
#include "error.h"
#include "integer.h"
#include "Hardware/CO2_MHZ.h"
#include "pm.h"

/* Exported types ------------------------------------------------------------*/
typedef enum
{
	mnuMain,
	mnuPM,
	mnuO3,
	mnuGraph,
	mnuClock,
	mnuSettings,
	mnuAbout,
	mnuFirst = mnuMain,
	mnuLast = mnuAbout,
} menuScreens;
	
	
/** @addtogroup Exported_types
* @{
*/
typedef enum
{
	Key_None  = 0,
	Key_Up,
	Key_Down,
	Key_OK,
} KeyState;

/* Exported constants --------------------------------------------------------*/

//#define SDS_PM_SENSOR TRUE
#define PM_PLANT_SENSOR TRUE

/* Exported variables ------------------------------------------------------ */
extern CO2_MHZ co2;
//extern PM_PLANT pm;
#ifdef SDS_PM_SENSOR
extern SDS_PM pm;
#else
extern PM_PLANT pm;
#endif

extern float pm_level;
extern float pm_level_25;
extern int co2_level;
extern __IO int32_t piezo_on_time;
extern __IO bool new_sensor_read, new_SD_save, new_co2_read, new_wifi_update, new_day, alarm_on, read_timer_;


/* Private functions ------------------------------------------------------- */
void SystemClock_Config(void);

void display_time(void);
void display_settings(void);
void green_led(bool state);
void menu_scroll(int8_t dir);
void short_beep(bool state, uint32_t time, uint32_t frequency);
extern "C" void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
void decode_time(RTC_TimeTypeDef* time, RTC_DateTypeDef* date, char * timeStr);

/* Exported macro ------------------------------------------------------------*/
#define green_led_state() HAL_GPIO_ReadPin(Green_LED_GPIO_Port, Green_LED_Pin)
#define wait_for_keypress()	while(key_status == Key_None){};
#define ARR_LEN(x) (sizeof(x)/sizeof(*x))


/*-------------------------------END OF FILE-------------------------------*/
