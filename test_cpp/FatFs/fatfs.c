/**
  ******************************************************************************
  * @file   fatfs.c
  * @brief  Code for fatfs applications
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "fatfs.h"
#include "main.h"
#include "stdint.h"
#include "string.h"

uint8_t retSD;    /* Return value for SD */
char SD_Path[4];  /* SD logical drive path */

/* USER CODE BEGIN Variables */
FATFS SDFatFs;			//File system object for SD card drive
FIL MyFile;				//File object

/* USER CODE END Variables */    

void MX_FATFS_Init(void) 
{
  /*## FatFS: Link the SD driver ###########################*/
	retSD = FATFS_LinkDriver(&SD_Driver, SD_Path);

	  /* USER CODE BEGIN Init */
	  /* additional user code for init */     

	    /* USER CODE END Init */
}

/**
  * @brief  Gets Time from RTC 
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
  /* USER CODE BEGIN get_fattime */
	return 0;
	/* USER CODE END get_fattime */  
}

int32_t check_sd_formatted(void)
{
	FRESULT res;
	uint32_t byteswritten, bytesread;
	char FileName[20] = "Log_1.txt";
	char someText[20] = "some text \n";

	//If disk IO driver linked OK
	if (retSD != 0)		
		return -1;		//could return string error? "driver io fail" etc.
	
		//Register the files system object to the FatFS module
	if (f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0) != FR_OK)
		return -1;
		
	//snprintf(FileName, 20, "Log_%s.txt", "23");
	//Create new file if it doesn't exist, open for writing
	if (f_open(&MyFile, FileName, FA_OPEN_EXISTING | FA_WRITE) != FR_OK)
	{
		if (f_open(&MyFile, FileName, FA_CREATE_NEW | FA_WRITE) != FR_OK)
			return -1;
		else
			write_header(&MyFile);	//New file is created so write header to top of file.
	}
		
	
	//Could do if file doesn't exist write header at top of file with Status,Temperature, etc.
	
	/* Move to end of the file to append data */
	res = f_lseek(&MyFile, f_size(&MyFile));
	
	res = f_write(&MyFile, "some text \n", strlen(someText), (void *)&byteswritten);
	
	if (f_close(&MyFile) != FR_OK)
		return -1;
	
	
	return 0;	//success
}

//Pass name of file
//pass data to write, etc.

void write_header(FIL * log_file)
{ 
	uint32_t bytes;
	
	char header[50] = "Header ---------------\n";
	f_write(log_file, header, strlen(header), (void *)&bytes);
}


/* USER CODE BEGIN Application */
//Write test seems to only run once, don't try to run twice
void write_test(void)
{
	FRESULT res;                                          /* FatFs function common result code */
	uint32_t byteswritten, bytesread;                     /* File write/read counts */
	uint8_t wtext[] = "This is STM32 working with FatFs"; /* File write buffer */
	uint8_t rtext[100];                                   /* File read buffer */
	uint8_t work_buf[_MAX_SS];
  
	
	  /*##-1- Link the micro SD disk I/O driver ##################################*/
	if (retSD == 0)
	{
	  /*##-2- Register the file system object to the FatFs module ##############*/
		if (f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0) != FR_OK)
		{
		  /* FatFs Initialization Error */
			Error_Handler();
		}
		else
		{
		  /*##-3- Create a FAT file system (format) on the logical drive #########*/
		  /* WARNING: Formatting the uSD card will delete all content on the device */
			if (f_mkfs((TCHAR const*)SD_Path, FM_FAT32, 0, work_buf, sizeof(work_buf)) != FR_OK)					//path, FAT format, default allocation size, working buffer
			{
			  /* FatFs Format Error */
				Error_Handler();
			}
			else
			{       
			  /*##-4- Create and Open a new text file object with write access #####*/
				if (f_open(&MyFile, "STM32.TXT", FA_CREATE_ALWAYS | FA_WRITE) != FR_OK)
				{
				  /* 'STM32.TXT' file Open for write Error */
					Error_Handler();
				}
				else
				{
				  /*##-5- Write data to the text file ################################*/
					res = f_write(&MyFile, wtext, sizeof(wtext), (void *)&byteswritten);

					          /*##-6- Close the open text file #################################*/
					if (f_close(&MyFile) != FR_OK)
					{
						Error_Handler();
					}
          
					if ((byteswritten == 0) || (res != FR_OK))
					{
					  /* 'STM32.TXT' file Write or EOF Error */
						Error_Handler();
					}
					else
					{      
					  /*##-7- Open the text file object with read access ###############*/
						if (f_open(&MyFile, "STM32.TXT", FA_READ) != FR_OK)
						{
						  /* 'STM32.TXT' file Open for read Error */
							Error_Handler();
						}
						else
						{
						  /*##-8- Read data from the text file ###########################*/
							res = f_read(&MyFile, rtext, sizeof(rtext), (UINT*)&bytesread);
              
							if ((bytesread == 0) || (res != FR_OK))
							{
							  /* 'STM32.TXT' file Read or EOF Error */
								Error_Handler();
							}
							else
							{
							  /*##-9- Close the open text file #############################*/
								f_close(&MyFile);
                
								/*##-10- Compare read data with the expected data ############*/
								if ((bytesread != byteswritten))
								{                
								  /* Read data is different from the expected data */
									Error_Handler();
								}
								else
								{
								  /* Success of the demo: no error occurrence */
									//BSP_LED_On(LED1);
									wtext[0] = 'a';
								}
							}
						}
					}
				}
			}
		}
	}
  
	/*##-11- Unlink the RAM disk I/O driver ####################################*/
	FATFS_UnLinkDriver(SD_Path);

}
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
