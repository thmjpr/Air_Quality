
/**
  ******************************************************************************
  * @file    graph.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "Fonts.h"
#include "sd_card.h"



class Graph
{
public:
	/**
	* @brief   List of graphs that can be displayed
	* @details 
	*/
	typedef enum
	{
		Temperature,
		Humidity,
		Pressure,
		CO,
		CO2,
		PM2,
		PM10,
		GRAPH_First = Temperature,
		GRAPH_Last = PM10,
	}Graph_type;

	/**
	* Graph constructor.
	*
	*/
	Graph();

	/**
	* Graph destructor.
	*/
	~Graph();

	/**
	* @brief  Draw the axis and tile of the graph
	* @param  Graph number
	* @retval
	*/
	void draw_graph(Graph_type graph_num);

	/**
	* @brief  
	* @param
	* @retval
	*/
	void update_graph(int value);

	/**
	* @brief
	* @param
	* @retval
	*/
	void update(SD_CARD::logData * log);


	void draw_temperature(float temperature);
	void draw_pm();


	uint16_t get_min(Graph_type type);
	uint16_t get_max(Graph_type type);
	uint16_t get_avg(Graph_type type);
	uint16_t get_cur(Graph_type type);
	
private:
	static const uint8_t GRAPH_W = (DISP_W - 40);
	static const uint8_t GRAPH_H = 40;
	const uint8_t X_YAXIS = 20;
	const uint8_t Y_XAXIS = 58;
	
	uint8_t plot[GRAPH_W + 1] = { 0 };
	int local_max = 0;
	
	//graph data
	bool fill_plot = false;			//draw line or solid bars
	bool compress_plot = true;		//compress horizontal data up to xxhrs

	uint16_t plot_buff[GRAPH_Last + 1][GRAPH_W] = { {0} };		//
	uint16_t plot_ptr;//[GRAPH_LAST+1];							//pointer within circular buffer
	//amount of data recorded?

};


/*-------------------------------END OF FILE-------------------------------*/




