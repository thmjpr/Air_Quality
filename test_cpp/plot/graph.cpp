/**
  ******************************************************************************
  * @file    graph.cpp
  * @author  Thomas
  * @version 
  * @date    
  * @brief   Graph log data onto a small area on the display
  ******************************************************************************
  * @attention
  * 
  * 
  ******************************************************************************
  */

/*
Update intervals:
30s
1min
5min
10min
1hr

define a maximum value? or let it autoscale to 10,20,50,100 whatever
should always update every 1 second?

for now can just have one graph, and reset it if a new sensor is selected.
later could save states, load, etc.

Can have screen with 6 graphs? or just 6 numbers maybe
*/


#include <stdio.h>
#include "SSD1306.h"
#include "graph.h"
#include "stdint.h"
#include "math.h"
#include "main.h"

//---------------------------------------------------------
//Graph titles
const uint8_t titles[][22] = { 
	{ "    Temperature (C)" },
	{ "       Humidity (%)" },
	{ "     Pressure (kPa)" },
	{ "   C monoxide (ppb)" },
	{ "    C dioxide (ppm)" },
	{ "     PM2.5 (ug/m3)" },
	{ "      PM10 (ug/m3)" },
};

//Graph axis min, max
int axis[][2] = { 
	{ 10, 40 },		//temp C
	{ 0, 100 },		//humid %
	{ 95, 105 },	//pres kPa
	{ 0, 5000 },	//CO ppm
	{ 300, 1100 },	//CO2 ppm
	{ 0, 50 },		//PM2.5
	{ 0, 50 },		//PM10
};

//******************************************************************************
Graph::Graph()
{
}

//******************************************************************************
Graph::~Graph()
{
}

//-----------------------------------------------------------------------------

void Graph::draw_graph(Graph_type graph_num)
{
	char buf[8];
	int i, k;
	uint16_t local_min, local_max, y;
	
	//Draw title
	ssd1306_display_string(0, 0, titles[graph_num], FONT_MED, 1);
		
	//Draw axis
	ssd1306_draw_line(X_YAXIS, 10, X_YAXIS, Y_XAXIS, 1);	//y axis
	ssd1306_draw_line(X_YAXIS, Y_XAXIS, X_YAXIS + GRAPH_W, Y_XAXIS, 1);	//x axis
	
	//Label axis
	sprintf(buf, "%3d", axis[graph_num][0]);	//min
	ssd1306_display_string(0, 50, (uint8_t*)buf, FONT_MED, 1);
	
	if(((axis[graph_num][0] + axis[graph_num][1])/2) > 1000)
		sprintf(buf, "%1dk", (axis[graph_num][0] + axis[graph_num][1])/2000);	//mid
	else
		sprintf(buf, "%3d", (axis[graph_num][0] + axis[graph_num][1])/2);	//mid
	ssd1306_display_string(0, 30, (uint8_t*)buf, FONT_MED, 1);
	
	//Can change to 1.2k
	//Save some space if number has 4 digits
	if (axis[graph_num][1] >= 1000)
		sprintf(buf, "%1dk", axis[graph_num][1] / 1000);	//max
	else	
		sprintf(buf, "%3d", axis[graph_num][1]);	//max

	ssd1306_display_string(0, 10, (uint8_t *)buf, FONT_MED, 1);
	
	ssd1306_draw_line(20, 36, 18, 36, 1);	//y tick
	
	//for(i=0;i++;i<num_ticks)
	ssd1306_draw_line(40, Y_XAXIS, 40, Y_XAXIS+2, 1);	//x tick
	ssd1306_draw_line(80, Y_XAXIS, 80, Y_XAXIS+2, 1);	//x tick
	
	//clear area of screen that graph occupies
	//ssd1306_fill_screen(X_YAXIS + 1, 10, 100, Y_XAXIS - 1, 0);
	
	local_max = get_max(graph_num);
	local_min = get_min(graph_num);
	
	//Can come up with better rounding, depends on the max/min range
	if (local_max > axis[graph_num][1])
		axis[graph_num][1] = axis[graph_num][1] + 100;
	
	local_max = axis[graph_num][1];
	
	//Draw new points
	for (i = 0; i < GRAPH_W; i++)
	{
		//Display points from beginning of circular buffer, loop at end
		k = i + plot_ptr;
		if (k >= GRAPH_W)
			k -= GRAPH_W;
		
		//scale value to height of screen
		y = plot_buff[graph_num][k];
		if (y > 0)
		{
			y = y * GRAPH_H / local_max;
		}
		
		//if get_max > axis[][] whatever then need to scale up axis[][] or ?
		
		
		if (fill_plot)
			ssd1306_draw_line(i + X_YAXIS, Y_XAXIS, i + X_YAXIS, Y_XAXIS - y, true); 	
		else
			ssd1306_draw_point(i + X_YAXIS, Y_XAXIS - y, true);		
	}
	

	switch (graph_num)
	{
		case Graph::Temperature:
		local_min = 0;
			break;
		case Graph::Humidity:
			break;
		case Graph::Pressure:
			break;
		case Graph::CO:
			break;
		case Graph::CO2:
			break;
		case Graph::PM2:
			break;
		case Graph::PM10:
			break;
	}
}



//-----------------------------------------------------------------------------
void Graph::draw_pm()
{
	char buf[20];

	//Draw current temp/humid
	sprintf(buf, "%3d", get_cur(PM2));
	ssd1306_display_string(10, 15, (uint8_t*)buf, FONT_LARGE, 1);

	sprintf(buf, "%3d", get_cur(PM10));
	ssd1306_display_string(75, 15, (uint8_t*)buf, FONT_LARGE, 1);

	//put degrees symbol


	//Draw min/max/avg
	/*
	ssd1306_display_string(3, 44, (uint8_t*)"min", FONT_MED, 1);
	ssd1306_display_string(3, 54, (uint8_t*)"max", FONT_MED, 1);

	sprintf(buf, "%2d %2d %2d", get_min(PM2), get_max(PM2));
	ssd1306_display_string(25, 47, (uint8_t*)buf, FONT_MED, 1);

	sprintf(buf, "%2d %2d %2d", get_min(PM10), get_max(PM10));
	ssd1306_display_string(25, 56, (uint8_t*)buf, FONT_MED, 1);

	//Display ug/m3
	ssd1306_draw_bitmap(0, 2, &icon_ugm3_small[0], 114, 47);
	*/
}

//-----------------------------------------------------------------------------
void Graph::draw_temperature(float temperature)
{
	char buf[20];

	//Draw current temp/humid
	sprintf(buf, "%0.1f C", temperature);
	ssd1306_display_string(10, 15, (uint8_t*)buf, FONT_LARGE, 1);

	sprintf(buf, "%2d %%", get_cur(Humidity));
	ssd1306_display_string(75, 15, (uint8_t*)buf, FONT_LARGE, 1);

	//put degrees symbol


	//Draw min/max/avg
	ssd1306_display_string(25, 36, (uint8_t*)"min   max   avg", FONT_MED, 1);

	sprintf(buf, "%2d %2d %2d", get_min(Temperature), get_max(Temperature), get_avg(Temperature));
	ssd1306_display_string(25, 47, (uint8_t*)buf, FONT_MED, 1);

	sprintf(buf, "%2d %2d %2d", get_min(Humidity), get_max(Humidity), get_avg(Humidity));
	ssd1306_display_string(25, 56, (uint8_t*)buf, FONT_MED, 1);
	
	ssd1306_display_string(118, 47, (uint8_t*)"C", FONT_MED, 1);
	ssd1306_display_string(118, 56, (uint8_t*)"%", FONT_MED, 1);
}

//-----------------------------------------------------------------------------
uint16_t Graph::get_min(Graph_type type)
{
	uint16_t smallest = plot_buff[type][0];

	for (uint32_t i = 1; i < ARR_LEN(plot_buff); ++i)
		if (plot_buff[type][i] < smallest)
			smallest = plot_buff[type][i];

	return smallest;
}

//-----------------------------------------------------------------------------
uint16_t Graph::get_max(Graph_type type)
{
	uint16_t biggest = plot_buff[type][0];

	for (uint32_t i = 1; i < ARR_LEN(plot_buff); ++i)
		if (plot_buff[type][i] > biggest)
			biggest = plot_buff[type][i];

	return biggest;
}

//-----------------------------------------------------------------------------
uint16_t Graph::get_avg(Graph_type type)
{
	uint32_t avg = 0;

	for (uint32_t i = 0; i < ARR_LEN(plot_buff); ++i)
		avg += plot_buff[type][i];

	return static_cast<uint16_t>(round((float)avg / ARR_LEN(plot_buff)));
}

//-----------------------------------------------------------------------------
uint16_t Graph::get_cur(Graph_type type)
{
	return plot_buff[type][plot_ptr];
}


//-----------------------------------------------------------------------------
/**
  * @brief  Update the datapoints on the graph and scale if needed
  * @param  
  * @retval  
  */

void Graph::update_graph(int value)
{
	int i, y;
	
	if (value > local_max)
		local_max = value;
	
	//scale value to height of screen
	if (value == 0)
		y = 0;
	else
		y = (value * DISP_H) / local_max;
	
	//Shift existing values to the left
	for (i = 0; i < GRAPH_W; i++)
		plot[i] = plot[i + 1];	
		
	plot[GRAPH_W] = value;
	
	//clear area of screen that graph occupies
	ssd1306_fill_screen(X_YAXIS + 1, 10, 100, Y_XAXIS - 1, 0);
	
	//Draw new points
	for (i = 0; i < GRAPH_W; i++)
	{
		if (fill_plot)
			ssd1306_draw_line(i+X_YAXIS, Y_XAXIS, i+X_YAXIS, Y_XAXIS - plot[i], true); 	
		else
			ssd1306_draw_point(i+X_YAXIS, Y_XAXIS - plot[i], true);		
	}
}

//-----------------------------------------------------------------------------
void Graph::update(SD_CARD::logData * log)
{
	
	static uint16_t plot_ptrz = 0;
	
	if (plot_ptrz >= GRAPH_W)			//annoying bug plot_ptr is resetting after 86 to 30? not sure why
		plot_ptrz = 0;

	plot_ptr = plot_ptrz;
	
	plot_buff[Temperature][plot_ptr] = static_cast<uint16_t>(round(log->temperature));
	plot_buff[Humidity][plot_ptr] = log->humidity;
	plot_buff[Pressure][plot_ptr] = static_cast<uint16_t>((log->pressure + 500)/1000);			//divide to get kPa
	plot_buff[CO][plot_ptr] = static_cast<uint16_t>(log->co);
	plot_buff[CO2][plot_ptr] = static_cast<uint16_t>(log->co2);
	plot_buff[PM2][plot_ptr] = static_cast<uint16_t>(round(log->pm2));
	plot_buff[PM10][plot_ptr] = static_cast<uint16_t>(round(log->pm10));
	
	plot_ptrz++;
}