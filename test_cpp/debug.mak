#Generated by VisualGDB (http://visualgdb.com)
#DO NOT EDIT THIS FILE MANUALLY UNLESS YOU ABSOLUTELY NEED TO
#USE VISUALGDB PROJECT PROPERTIES DIALOG INSTEAD

BINARYDIR := Debug

#Additional flags
PREPROCESSOR_MACROS := DEBUG=1 STM32L476xx=1 DEBUG_DEFAULT_INTERRUPT_HANDLERS USE_FULL_ASSERT=1
INCLUDE_DIRS := OLED Hardware/SSD1306 Hardware/SPI Hardware/IIC Hardware/delay C:/STMCubeHAL/STM32Cube_FW_L4_V1.5.0/Drivers/STM32L4xx_HAL_Driver/Inc C:/STMCubeHAL/STM32Cube_FW_L4_V1.5.0/Drivers/CMSIS/Include C:/STMCubeHAL/STM32Cube_FW_L4_V1.5.0/Drivers/CMSIS/Device/ST/STM32L4xx/Include C:/STMCubeHAL/STM32Cube_FW_L4_V1.5.0/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy C:/STMCubeHAL/STM32Cube_FW_L4_V1.5.0/Drivers/CMSIS/RTOS/Template plot FatFs FatFs/src FatFs/src/drivers Hardware USB/STM32_USB_Device_Library/Core/Inc USB/STM32_USB_Device_Library/Class/MSC/Inc USB
LIBRARY_DIRS := 
LIBRARY_NAMES := 
ADDITIONAL_LINKER_INPUTS := 
MACOS_FRAMEWORKS := 
LINUX_PACKAGES := 

CFLAGS := -ggdb -ffunction-sections -Og -fno-exceptions -Wall
CXXFLAGS := -ggdb -ffunction-sections -Og -fno-exceptions -Wall
ASFLAGS := 
LDFLAGS := -Wl,-gc-sections
COMMONFLAGS := 
LINKER_SCRIPT := 

START_GROUP := -Wl,--start-group
END_GROUP := -Wl,--end-group

#Additional options detected from testing the toolchain
USE_DEL_TO_CLEAN := 1
CP_NOT_AVAILABLE := 1

ADDITIONAL_MAKE_FILES := stm32.mak
GENERATE_BIN_FILE := 1
GENERATE_IHEX_FILE := 0
