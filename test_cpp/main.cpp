/**
  ******************************************************************************
  * @file    main.cpp
  * @author  Thomas Price
  * @version 
  * @date    December, 2016
  * @brief   Main air quality application
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "error.h"
#include "sd_card.h"
#include "config_settings.h"
#include "graph.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"
#include "timer.h"
#include "rtc.h"
#include "adc.h"
#include "i2c.h"
#include "spi.h"
//#include "USB/usb_device.h"
#include "OLED/ssd1306_config.h"
#include "Hardware/BMP085_pressure.hpp"
#include "Hardware/si7020_humid.h"
#include "Hardware/LMP91000.h"
//#include "Hardware/CO_110_Spec.h"
#include "Hardware/O3_Spec.h"
#include "Hardware/ESP8266_wifi.h"

/* Private variables ---------------------------------------------------------*/
//Volatiles
__IO bool new_sensor_read = false, new_SD_save = false, new_co2_read = false, new_wifi_update = false, new_day = false, alarm_on = false, read_timer_ = false;
__IO KeyState key_status = Key_None;
__IO int32_t piezo_on_time = 0;

//--------------------------------------------------------------------------
//Sensor and communication objects
BMP085 bmp;
Si7020 humid;
//SPEC_CO_110 co;
SPEC_O3 co;						//TESTING 
ESP8266 wifi(&huart1);
#ifdef SDS_PM_SENSOR
SDS_PM pm(&huart2);
#else
PM_PLANT pm(&huart2);		//plantower type
#endif
CO2_MHZ co2(&huart3);
SD_CARD sd;
CONFIG cfg;

//
int  co_level, co2_level;
float pm_level, pm_level_25;
menuScreens menu = mnuMain;
#define BEEP_ON 0
#define USB 0

//================================================================================
// 
int main(void)
{
	char build_date[] = __DATE__;
	Graph grph;
	Graph::Graph_type graph_num = Graph::Temperature;
	SD_CARD::logData log;
	uint8_t entering_settings = 0;
	bool key_ok_pressed = false;
	float ambient_temp, humidity, pressure;
	uint8_t buff[50];
	
	/* MCU Configuration----------------------------------------------------------*/
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();
	
	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_SPI2_Init();
	MX_DMA_Init();
	
	MX_USART1_Init(115200);		//WiFi
	MX_USART2_Init();			//PM
	MX_USART3_Init(115200);		//CO2
	//MX_USB_DEVICE_Init();
	MX_I2C1_Init();
	MX_ADC1_Init();
	
	MX_TIM1_Init();
	MX_TIM2_Init();
	MX_TIM16_Init();
	
	MX_RTC_Init();			//Configure real-time clock (base date is set)
	OLED_init();			//OLED initialization

	//can config to read battery voltage
	//can implement something if sensor disconnected during testing, either keep reading or stop, etc.
	//Careful with the Rx Tx pins on this uart when module is powered off. If ever implementing sleep with pm/co2 off.
	//Should ignore the first 30s of data as its not accurate. CO even longer.
	
	//TODO:
	//**Mixza 8gb sd card still not working, it was working at some point?
	//method to show/input voltage reference voltage + CO sensor sensitivity. 6.20nA/ppm rating of sensor.
	
	//Can add PVD, for low voltage detection (and close SD card at that time?)
	//HAL_PWR_ConfigPVD()
	
	//use static_cast<uint8_t>() instead of c cast
	//can use std::array
	
	//hard fault if button pressed prior to loading
	
	//-----------------------------------------------------
	srand(0x55);
	
	//Starting up
	ssd1306_rotate_screen(true);
	ssd1306_clear_screen(0);
	ssd1306_draw_bitmap(0, 0, &icon_splash_screen[0], 128, 64);			//Splash screen
	ssd1306_update();
	wifi.power(true);													//early start wifi, as it takes ~6s+ to boot
	HAL_Delay(1900);
	ssd1306_clear_screen(0);
	
	//List modules available
	ssd1306_display_string(0, 0, (uint8_t*)"SD card   ?", FONT_MED, 1);
	ssd1306_display_string(0, 15, (uint8_t*)"Pressure  ?", FONT_MED, 1);
	ssd1306_display_string(0, 30, (uint8_t*)"Humidity  ?", FONT_MED, 1);
	ssd1306_display_string(0, 45, (uint8_t*)"WiFi      ?", FONT_MED, 1);
	ssd1306_display_string(80, 0, (uint8_t*)"CO/O3 ?", FONT_MED, 1); 
	ssd1306_display_string(80, 15, (uint8_t*)"CO2   ?", FONT_MED, 1);
	ssd1306_display_string(80, 30, (uint8_t*)"PM    ?", FONT_MED, 1);
	ssd1306_display_string(80, 45, (uint8_t*)"Ext   ?", FONT_MED, 1);
	ssd1306_update();
		
	//--------------------------------------------------
	//Checking for modules
	// SD card
	if (sd.init())
	{
		if (sd.check_sd_formatted(&cDate) == 0)
		{
			ssd1306_display_string(0, 0, (uint8_t*)"SD card  OK", FONT_MED, 1);		//
			sd.load_config_file(&cfg);
		}
		else		//SD not formatted
		{
			ssd1306_clear_screen(0);
			ssd1306_display_string_up(0, 0, (uint8_t*)"SD card not formatted", FONT_MED, 1);
			//ssd1306_display_string(0, 0, (uint8_t*)"Press OK to format, down to skip", FONT_MED, 1); 
			
			wait_for_keypress();
			if (key_status == Key_OK)
			{
				//sd.format();
			}
		}
	}
	else
	{
		ssd1306_display_string_up(0, 0, (uint8_t*)"SD card   X", FONT_MED, 1);
		//cfg load_defaults or maybe it will always do this
	}

	//Initialize BMP085 pressure/temperature sensor
	if (bmp.begin())
	{
		ambient_temp = bmp.readTemperature();
		pressure = bmp.readPressure();
		ssd1306_display_string_up(0, 15, (uint8_t*)"Pressure OK", FONT_MED, 1);
	}
	else
		ssd1306_display_string_up(0, 15, (uint8_t*)"Pressure  X", FONT_MED, 1);
	
	//Initialize humidity sensor
	if (humid.init() == 0)
	{
		humid.getHumidity(&ambient_temp);
		humid.getTemperature(&ambient_temp);
		ssd1306_display_string_up(0, 30, (uint8_t*)"Humidity OK", FONT_MED, 1);
	}
	else
		ssd1306_display_string_up(0, 30, (uint8_t*)"Humidity  X", FONT_MED, 1);

	//Initialize ESP8266
	if (wifi.init())
	{
		wifi.get_mac();
		ssd1306_display_string_up(0, 45, (uint8_t*)"WiFi     OK", FONT_MED, 1);
	}
	else
		ssd1306_display_string_up(0, 45, (uint8_t*)"WiFi      X", FONT_MED, 1);

	//Initialize CO sensor
	if (co.init() == 0)
	{
		co_level = co.read_blocking(ambient_temp);
		//**FFF implement i2c timeouts for sure.. assume if LMP is present we have CO
		ssd1306_display_string_up(80, 0, (uint8_t*)"CO    OK", FONT_MED, 1); 
	}
	else
		ssd1306_display_string_up(80, 0, (uint8_t*)"CO    X", FONT_MED, 1); 
	
	//Initialize CO2 sensor
	if (co2.init())
	{
		co2.request();
		ssd1306_display_string_up(80, 15, (uint8_t*)"CO2   OK", FONT_MED, 1);
	}
	else
		ssd1306_display_string_up(80, 15, (uint8_t*)"CO2   X", FONT_MED, 1);
	
	//Initialize particulate matter sensor
	if (pm.init())
	{
		pm.request();
		ssd1306_display_string_up(80, 30, (uint8_t*)"PM    OK", FONT_MED, 1);
	}
	else
		ssd1306_display_string_up(80, 30, (uint8_t*)"PM    X", FONT_MED, 1);
	
	//If no 5V sensors are present, power down 5V output
	if (!pm.is_present() & !co2.present)
		pm.power(false);
	
	HAL_Delay(500);
	
	ssd1306_display_string_up(80, 45, (uint8_t*)"Ext   X", FONT_MED, 1);
	read_timer(true);				//Start the 1s read timer interrupt
	HAL_Delay(500);
	
	//-----------------------------------------------------------------------------------
	// Main loop
	while (1)
	{
		//-------------------------------------------------------------------------------
		//User interface
		ssd1306_clear_screen(0);		//clear screen buffer
		
		if (key_status)		//If key pressed
		{
			short_beep(true, 10, 0);
			
			if (key_status == Key_Up)
			{
				menu_scroll(-1);
				entering_settings = 0;
			}
			else if (key_status == Key_Down)
			{
				menu_scroll(1);
				entering_settings = 0;
			}
			else if (key_status == Key_OK)
			{
				key_ok_pressed = true;
				//alarm_on = false; will just keep enabling
			}

			key_status = Key_None;
		}



		//Display a menu screen
		switch (menu)
		{			
		case mnuMain:
						//Temperature
			sprintf((char*)buff, "%0.1f C", log.temperature);
			ssd1306_display_string(35, 0, buff, FONT_LARGE, 1);
	
			//Humidity
			sprintf((char*)buff, "%02d.0 %%", log.humidity);
			ssd1306_display_string(35, 20, buff, FONT_LARGE, 1);
	
	
			//CO2
			ssd1306_display_string(0, 20 + 15, (uint8_t*)"CO2:", FONT_LARGE, 1); 
			if (!co2.present)
				sprintf((char*)buff, "- ppm");
			else
				sprintf((char*)buff, "%4d ppm", (int)log.co2);
			ssd1306_display_string(40, 20 + 15, buff, FONT_LARGE, 1);
			

			if (log.co2 > 1900)
			{
				sprintf((char*)buff, "*DANGER*");
				ssd1306_display_string(25, 20 + 30, buff, FONT_LARGE, 1);
			}	
			else if (log.co2 > 1800)
			{
				sprintf((char*)buff, "*WARNING*");
				ssd1306_display_string(25, 20 + 30, buff, FONT_LARGE, 1);
			}
			else
			{
			}
			
			//activity dot
			if (green_led_state())
			{
				sprintf((char*)buff, ".");
				ssd1306_display_string(0, 50, buff, FONT_MED, 1);	
			}
			

			break;
			
			
			/*
			//Temperature
			sprintf((char*)buff, "%0.1f C", log.temperature);
			ssd1306_display_string(25, 0, buff, FONT_LARGE, 1);
	
			//Humidity
			sprintf((char*)buff, "%02d%%", log.humidity);
			ssd1306_display_string(85, 0, buff, FONT_LARGE, 1);
			
			//Pressure
			//sprintf((char*)buff, "P %3dkPa", log.pressure/1000);
			//ssd1306_display_string(0, 15, buff, FONT_MED, 1);
			
			//CO or O3
			ssd1306_display_string(0, 20, (uint8_t*)"O3:", FONT_MED, 1); 
			if (!co.present)
				sprintf((char*)buff, " - ppm");
			else if (abs(co_level) > (100 * 1000))
				sprintf((char*)buff, "%0.0f ppm", (float)co_level / 1000.0);
			else if (abs(co_level) > (10 * 1000))		
				sprintf((char*)buff, "%0.1f ppm", (float)co_level / 1000.0);
			else
				sprintf((char*)buff, "%0.0f ppb", (float)co_level);
			ssd1306_display_string(46, 20, buff, FONT_MED, 1);
			
			//CO2
			ssd1306_display_string(0, 20+15, (uint8_t*)"CO2:", FONT_MED, 1); 
			if (!co2.present)
				sprintf((char*)buff, "- ppm");
			else
				sprintf((char*)buff, "%4d ppm", (int)log.co2);
			ssd1306_display_string(40, 20+15, buff, FONT_MED, 1);
			
			//PM 1.0/2.5/10
			if (!pm.is_present())
				ssd1306_display_string(0, 20 + 30, (uint8_t*)"PM: -", FONT_MED, 1); 
			else
			{	
				ssd1306_display_string(0, 20 + 30, (uint8_t*)"PM:", FONT_MED, 1); 
		
				//If PMS7003 sensor with pm1/2/10
				#ifndef SDS_PM_SENSOR
					sprintf((char*)buff, "%1.0f, %1.0f, %1.0f", log.pm1, log.pm2, log.pm10);
					ssd1306_display_string(25, 20 + 30, buff, FONT_MED, 1);
				#else
					if (log.pm2 > 1000)
						sprintf((char*)buff, "%0.0f,", log.pm2);
					else
						sprintf((char*)buff, "%0.1f,", log.pm2);
					ssd1306_display_string(35, 20 + 30, buff, FONT_MED, 1);
			
					if (log.pm10 > 1000)
						sprintf((char*)buff, "%0.0f", log.pm10);
					else		
						sprintf((char*)buff, "%0.1f", log.pm10);
					ssd1306_display_string(70, 20 + 30, buff, FONT_MED, 1);
				#endif
					
			}
			ssd1306_draw_bitmap(100, 64-12, &icon_ugm3_small[0], 8, 12);			//units
			break;
*/
			//
		case mnuPM:
			static bool aqi_mode = false;
			if (key_ok_pressed)	//switch between ug and AQI
				aqi_mode = !aqi_mode;
				
			//ssd1306_set_contrast(255);
			sprintf((char*)buff, "PM2.5");
			ssd1306_display_string(15, 35, buff, FONT_MED, 1);
			sprintf((char*)buff, "PM10");
			ssd1306_display_string(72, 35, buff, FONT_MED, 1);
			
			sprintf((char *)buff, "%3u", (uint)(log.pm2 + 0.5));
			ssd1306_draw_3216str(0, 0, buff);
			
			sprintf((char *)buff, "%3u", (uint)(log.pm10 + 0.5));
			ssd1306_draw_3216str(60, 0, buff);

			if (aqi_mode)
				ssd1306_display_string(115, 13, (uint8_t*)"AQI", FONT_MED, 1);		//AQI index, ranges from 0 to 500. Not a true 24hr calculated value
			else
				ssd1306_draw_bitmap(118, 13, &icon_ugm3_small[0], 8, 12);			//units		
	//ssd1306_draw_bitmap(100, 1, &icon_ugm3[0], 8, 18);				//units large not working?

			
				//CO2
			if (!co2.present)
				sprintf((char*)buff, "CO2: -");
			else
				sprintf((char*)buff, "CO2: %4d", (int)log.co2);
			ssd1306_display_string(57, 50, buff, FONT_MED, 1);
			
			//Temperature
			sprintf((char *)buff, "%0.0fC", log.temperature);
			ssd1306_display_string(0, 50, buff, FONT_MED, 1);
	
			//Humidity
			sprintf((char*)buff, "%02d%%", log.humidity);
			ssd1306_display_string(30, 50, buff, FONT_MED, 1);
			break;
			
		case mnuO3:
			sprintf((char*)buff, "O3 ppb");
			ssd1306_display_string(15, 35, buff, FONT_MED, 1);
			
			sprintf((char *)buff, "%3u", (unsigned int)(log.co));
			ssd1306_draw_3216str(0, 0, buff);
			break;

			
		case mnuClock:
			static bool ntp_checked = false;
			char time[32];
			
			if (ntp_checked == false)				//Check time once per power cycle
			{
				wifi.get_time(time);
				decode_time(&nTime, &nDate, time);
				if(nDate.Year < 70)					//Avoid setting invalid date (0000)
					rtc_set_time_and_date(&nTime, &nDate);
				ntp_checked = true;
			}
			display_time();
			break;
			
		case mnuGraph:
			//iterate through the graph types
			if (key_ok_pressed)
			{
				if (graph_num == Graph::GRAPH_Last)
					graph_num = Graph::GRAPH_First;
				else
					graph_num = static_cast<Graph::Graph_type>(static_cast<int>(graph_num) + 1);
			}
			
			grph.draw_graph(graph_num);
			
			//Can skip graph if sensor is not connected
			switch (graph_num)
			{
			case Graph::Temperature:
				break;
			case Graph::Humidity:
				break;
			case Graph::Pressure:
				break;
			case Graph::CO:
				break;
			case Graph::CO2:
				break;
			case Graph::PM2:
				break;
			case Graph::PM10:
				break;
			}
			break;
			
		case mnuSettings:
			//Show settings message, then after timeout show list of settings
			if (entering_settings < 10)
			{
				ssd1306_display_string(25, 0, (uint8_t*)"Settings", FONT_LARGE, 1);
				entering_settings++;
			}
			else
			{
				display_settings();
			}
			
			//Option to prepare SD card for removal, option to pause readings?
			//please remove SD card. press OK when new SD inserted. -> check SD etc
			//Please power off (?) then remove SD card
			break;
			
		case mnuAbout:
			ssd1306_display_string(0, 0, (uint8_t*)"Air quality monitor", FONT_MED, 1); 
			ssd1306_display_string(0, 20, (uint8_t*)"Build:", FONT_MED, 1); 
			ssd1306_display_string(40, 20, (uint8_t*)build_date, FONT_MED, 1); 
			ssd1306_display_string(0, 40, (uint8_t*)"Thomas Price", FONT_MED, 1);
			break;
		
		default:
			break;
		}
		
		//Draw arrows depending on menu screen
		if (menu == mnuFirst)
			ssd1306_draw_bitmap(121, 58, &icon_down_arrow[0], 7, 6);
		else if (menu == mnuLast)
			ssd1306_draw_bitmap(121, 0, &icon_up_arrow[0], 7, 6);
		else
		{
			ssd1306_draw_bitmap(121, 0, &icon_up_arrow[0], 7, 6);
			ssd1306_draw_bitmap(121, 58, &icon_down_arrow[0], 7, 6);
		}
		
		key_ok_pressed = false;
		ssd1306_update();
		green_led(!green_led_state());
		delay_ms(50);		//could have a delay_ms_brk which exits when button is pressed. or update based on tick (if %50 etc.)
		
		//-------------------------------------------------------------------------------
		//Logging
		
		if (new_sensor_read)
		{
			//Get the last readings + update graphs
			rtc_get_time_and_date(&log.time, &log.date);
			
#ifndef SDS_PM_SENSOR
			log.pm1 = pm.get_pm1_level();				  
#endif 
			log.pm2 = pm.get_pm25_level();
			log.pm10 = pm.get_pm10_level();
			log.co = co_level;
			log.co2 = co2.get_co2_level();
			log.humidity = humidity;
			log.temperature = ambient_temp;
			log.pressure = pressure;
			
			//check for high levels of pm/co/co2
			if (cfg.audio_alarm)
			{
				if (log.pm2 > cfg.pm2_alarm)
					alarm_on = true;
				else if (log.pm10 > cfg.pm10_alarm)
					alarm_on = true;
				else if (log.co > cfg.co_alarm_ppm * 1000)		//co in PPB
					alarm_on = true;
				else if (log.co2 > cfg.co2_alarm_ppm)
					alarm_on = true;
				else
					alarm_on = false;
			}

			if (sd.present & new_SD_save)
				sd.save_log(&log);
			
			grph.update(&log);		//Update display graphs with new data

			//Start new readings
			if (pm.is_present() && new_co2_read)			//do slower PM updates
				pm.request();
			if (humid.present)
			{
				humid.getHumidity(&humidity);
				humid.getTemperature(&ambient_temp);
				ambient_temp -= 3.0;						//Offset due to self heating (power supply), can calibrate
			}
			if (bmp.present)
				pressure = bmp.readPressure();			
			if (co.present)
				co_level = co.read_blocking(ambient_temp);	//return ppb
			if (co2.present && new_co2_read)
				co2.request();
			
			new_sensor_read = false;
			new_co2_read = false;
			new_SD_save = false;
		}
		
		if (new_wifi_update)
		{
			//send data to esp (all even if not valid, pm, temp, humid, etc, time?)
			wifi.send_measurements(log.temperature, log.humidity, log.pressure, log.pm1, log.pm2, log.pm10, log.co2, log.co, 0);
			new_wifi_update = false;
		}
		
		//If date = next day, start a new log file
		if (new_day)
		{
			new_day = false;
			//sd.shutdown();
			//sd.check_sd_formatted();
		}
	}
}


void estimate_current_aqi_pm25(float ug_conc)
{
	
	
	
}

//=========================================================
// Once button is pressed, interrupt will come here
// Could activate another interrupt for button released
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == Button_left_Pin)
		key_status = Key_Up;
	else if (GPIO_Pin == Button_right_Pin)
		key_status = Key_Down;
	else if (GPIO_Pin == Button_OK_Pin)
		key_status = Key_OK;
	else
		Error_Handler();
}

/** System Clock Configuration */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE | RCC_OSCILLATORTYPE_MSI;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;			//Low speed external crystal 
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;			//Mid speed internal osc
	RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 40;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
		Error_Handler();
	
	RCC_ClkInitStruct.ClockType =		RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK	| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource =	RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider =	RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider =	RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider =	RCC_HCLK_DIV1;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
		Error_Handler();
	
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_USART3 | RCC_PERIPHCLK_I2C1 | RCC_PERIPHCLK_SDMMC1 | RCC_PERIPHCLK_ADC | RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USB;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
	PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;				//External 32.768 crystal
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 24;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
		Error_Handler();
	
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)		//Main internal voltage regulator
		Error_Handler();

	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);					//Systick interrupt time (1ms)
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);				//
	HAL_RCCEx_EnableMSIPLLMode();										//MSI auto calibration

	  /* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}


/**
* @brief  Display settings menu screen
* @param  None
* @retval None
*/
void display_settings(void)
{
	if (cfg.button_beep)
		ssd1306_display_string(0, 0, (uint8_t*)"Key beep: ON", FONT_MED, 1);
	else
		ssd1306_display_string(0, 0, (uint8_t*)"Key beep: OFF", FONT_MED, 1);

	if (cfg.audio_alarm)
		ssd1306_display_string(0, 15, (uint8_t*)"Audio alarm: ON", FONT_MED, 1);
	else
		ssd1306_display_string(0, 15, (uint8_t*)"Audio alarm: OFF", FONT_MED, 1);

	ssd1306_display_string(0, 30, (uint8_t*)"Serv: ", FONT_MED, 1);
	ssd1306_display_string(30, 30, (uint8_t*)cfg.server, FONT_MED, 1);

	ssd1306_display_string(0, 45, (uint8_t*)"SSID: ", FONT_MED, 1);
	ssd1306_display_string(30, 45, (uint8_t*)cfg.wifi_ap, FONT_MED, 1);
}


/**
  * @brief  Display time and date on the LCD
  * @param  None
  * @retval None
  */	
void display_time(void)
{ 
	uint8_t buff[50];

	if (sd.present)
		ssd1306_draw_bitmap(0, 0, &icon_download[0], 8, 8);		//If SD card present put symbol

	if (cfg.audio_alarm)
		ssd1306_draw_bitmap(12, 0, &c_chAlarm88[0], 8, 8);		//If alarm enabled, put symbol
	
	if (wifi.present)
		ssd1306_draw_bitmap(24, 0, &c_chSingal816[0], 16, 8);	//If Wifi connected, put symbol //check width

	HAL_RTC_GetTime(&hrtc, &cTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &cDate, RTC_FORMAT_BIN);
	
	
	/* Display time Format : hh:mm:ss */
	sprintf((char *)buff, "%02d:%02d:%02d", cTime.Hours, cTime.Minutes, cTime.Seconds);
	ssd1306_draw_3216str(0, 16, buff);
	
	/* Display date Format : mm-dd */
	sprintf((char*)buff, "20%02d-%02d-%02d", cDate.Year, cDate.Month, cDate.Date);
	ssd1306_display_string(30, 50, buff, FONT_MED, 1);
}

/**
  * @brief  Output of green LED next to processor
  * @param  On or Off state of LED
  * @retval None
  */

void green_led(bool state)
{
	if (state)
		HAL_GPIO_WritePin(Green_LED_GPIO_Port, Green_LED_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(Green_LED_GPIO_Port, Green_LED_Pin, GPIO_PIN_RESET);
}

/**
  * @brief  Put processor in low power sleep mode
  * @param  None
  * @retval None
  */
void sleep(void)
{
	//Put GPIO in low power mode
	
	//Suspend tick to prevent wake from interrupt
	//could try wake from event too (difference?)
	HAL_SuspendTick();
	HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI); 
	//HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
	HAL_ResumeTick();
}

/**
  * @brief		Scroll up or down in the menu
  * @param dir: Direction to scroll the menu (- or +)
  * @retval		None
  */
void menu_scroll(int8_t dir)
{
	int i = (int) menu + dir;
	
	if (i < 0)					//Bottom of menu
		i = 0;					//i = (int)mnuSettings;
	
	else if (i > (int)mnuLast) //Top of menu
		i = (int)mnuLast;	//i = (int)mnuMain;

	menu = (menuScreens)i;
}

/**
  * @brief  Activate piezo buzzer for short beep
  * @param  state: 
  * @param  time:
  * @param  frequency:
  * @retval None
  */
void short_beep(bool state, uint32_t time, uint32_t frequency)
{
#if	BEEP_ON
	HAL_TIM_Base_Start_IT(&htim2);						//start timeout timer
	HAL_TIM_OC_Start(&htim16, TIM_CHANNEL_1);			//start piezo buzzer
#endif
	piezo_on_time = time;	
}


/**
  * @brief  Decode time string into RTC structure values
  * @param  None
  * @retval None
  * Format: Time: 2019-02-18T09:05:35 = xxYY-MM-DDxHH:MM:SS
  */
void decode_time(RTC_TimeTypeDef* time, RTC_DateTypeDef* date, char * timeStr)
{
	date->Year = atoi(timeStr + 2);		//2-digit year
	date->Month = atoi(timeStr + 2 + 3);
	date->Date = atoi(timeStr + 2 + 3 + 3);
	date->WeekDay = RTC_WEEKDAY_MONDAY;		//Weekday definition not used right now
	timeStr = timeStr + 11;
	time->Hours = atoi(timeStr);
	time->Minutes = atoi(timeStr + 3);
	time->Seconds = atoi(timeStr + 3 + 3);
	
	time->DayLightSaving = RTC_DAYLIGHTSAVING_SUB1H;
	time->StoreOperation = RTC_STOREOPERATION_RESET;
}

/**
* @brief Reports the name of the source file and the source line number
* where the assert_param error has occurred.
* @param file: pointer to the source file name
* @param line: assert_param error line source number
* @retval None
	   */
#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
	printf("Wrong parameters value: file %s on line %d\r\n", file, line);
	while (1)
	{
	}
}
#endif


/***************************END OF FILE****/