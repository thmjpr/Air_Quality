/**
  ******************************************************************************
  * @file    timer.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#pragma once

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"

extern TIM_HandleTypeDef htim1, htim2, htim16;
extern void Error_Handler(void);

extern void MX_TIM1_Init(void);
extern void MX_TIM2_Init(void);
extern void MX_TIM16_Init(void);
extern void read_timer(bool start);