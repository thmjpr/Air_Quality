
/**
  ******************************************************************************
  * @file    spi.c
  * @author  Thomas Price
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "spi.h"
#include "main.h"

//--------------------------------------------------------------------------
//Peripheral handles
SPI_HandleTypeDef hspi2;

/* Functions ------------------------------------------------------------------*/

/* SPI2 init function */
void MX_SPI2_Init(void)
{
	hspi2.Instance =		SPI2;
	hspi2.Init.Mode =		SPI_MODE_MASTER;
	hspi2.Init.Direction =	SPI_DIRECTION_2LINES;
	hspi2.Init.DataSize =	SPI_DATASIZE_8BIT;
	hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
	hspi2.Init.CLKPhase =	SPI_PHASE_2EDGE;		//whats this..
	hspi2.Init.NSS =		SPI_NSS_SOFT;
	hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;	//Spec for SSD1306 is 10MHz, 40/8 = 5MHz
	hspi2.Init.FirstBit =	SPI_FIRSTBIT_MSB;
	hspi2.Init.TIMode =		SPI_TIMODE_DISABLE;
	hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi2.Init.CRCPolynomial = 7;
	hspi2.Init.CRCLength =	SPI_CRC_LENGTH_DATASIZE;
	//**FF changed aug 12	hspi2.Init.NSSPMode =	SPI_NSS_SOFT;
	hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLED;
	
	if (HAL_SPI_Init(&hspi2) != HAL_OK)
		Error_Handler();
}